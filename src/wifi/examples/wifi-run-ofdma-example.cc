/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/core-module.h"
#include "ns3/gnuplot.h"
#include <list>
#include <vector>
#include <cstdlib>
#include <map>
#include <algorithm>

using namespace ns3;

/**
 * This script runs multiple instances of the wifi-ofdma example, draws plots
 * (saved as .eps files) and generates a complete report in PDF format.
 *
 * Usage: ./waf --run "wifi-run-ofdma-example --file=path/to/configFile"
 *
 * The following tools need to be found in a dir of the PATH env variable:
 * - waf
 * - gnuplot
 * - pdflatex
 *
 * configFile is a config file containing the options to pass to wifi-ofdma,
 * one option per line (empty lines or lines beginning with '#' are skipped),
 * with the value separated from the option by blank spaces:
 *
 * simulationTime 20
 * radius 10
 * channelWidth 40
 *
 * One line only in the config file can start with (x):
 *
 * (x) nHeStations 5 10 15 20 25 30
 *
 * Multiple values can be specified on such a line, each corresponding to a
 * different simulation run. Plots will have such values shown on the x-axis.
 * Other options can assume distinct values in the different simulation runs
 * if the corresponding lines start with (a):
 *
 * (a) trafficFile load5 load10 load15 load20 load25 load30
 *
 * The number of values specified on a line starting with (a) must match the
 * number of values specified on the line starting with (x).
 *
 * One line only in the config file can start with (y):
 *
 * (y) dlAckType NO-OFDMA ACK-SU-FORMAT MU-BAR AGGR-MU-BAR
 *
 * Plots will show a curve for each of the values specified on such a line.
 * Such values will be shown in a legend.
 *
 * Plots and report will be available in the directory containing the config file.
 * The files containing the traffic specs (trafficFile option) must be in the same
 * directory as the config file.
 */

std::string confFileName;

typedef std::map<std::size_t /* (y) */, Gnuplot2dDataset> DatasetMap;

DatasetMap throughput, throughputDl, throughputUl;
std::map<std::string /* AC*/, DatasetMap> perAcThroughputDl, perAcThroughputUl;

std::map<std::string /* AC*/, DatasetMap> perAcLossRateDl, perAcLossRateUl;

DatasetMap expiredDl, expiredUl;
std::map<std::string /* AC*/, DatasetMap> perAcExpiredDl, perAcExpiredUl;

DatasetMap rejectedDl, rejectedUl;
std::map<std::string /* AC*/, DatasetMap> perAcRejectedDl, perAcRejectedUl;

DatasetMap txFailuresDl, txFailuresUl;
std::map<std::string /* AC*/, DatasetMap> perAcTxFailuresDl, perAcTxFailuresUl;

std::map<std::string /* AC*/, DatasetMap> perAcDroppedByQdiscDl, perAcDroppedByQdiscUl;
std::map<std::string /* AC*/, DatasetMap> perAcMedianL2LatencyDl, perAcMedianL2LatencyUl;
std::map<std::string /* AC*/, std::map<std::size_t /* (x) */, DatasetMap>> perAcL2LatencyDlEcdf, perAcL2LatencyUlEcdf;
std::map<std::string /* AC*/, DatasetMap> perAcMedianE2eLatencyDl, perAcMedianE2eLatencyUl;
std::map<std::string /* AC*/, std::map<std::size_t /* (x) */, DatasetMap>> perAcE2eLatencyDlEcdf, perAcE2eLatencyUlEcdf;
std::map<std::string /* AC*/, DatasetMap> perAcMedianAggregateHolDl;
std::map<std::string /* AC*/, std::map<std::size_t /* (x) */, DatasetMap>> perAcAggregateHolDlEcdf;
std::map<std::string /* AC*/, DatasetMap> perAcMedianPairwiseHolDl;
std::map<std::string /* AC*/, std::map<std::size_t /* (x) */, DatasetMap>> perAcPairwiseHolDlEcdf;
std::map<std::string /* AC*/, DatasetMap> perAcMedianQdiscSojournDl, perAcMedianQdiscSojournUl;
std::map<std::string /* AC*/, std::map<std::size_t /* (x) */, DatasetMap>> perAcQdiscSojournDlEcdf, perAcQdiscSojournUlEcdf;

std::list<std::vector<std::string>> options;
typedef std::list<std::vector<std::string>>::iterator OptionIterator;
OptionIterator xParam = options.end (), yParam = options.end ();
std::vector<OptionIterator> aParams; // options whose values vary with x

std::map<std::size_t /* (x) */, std::map<std::size_t /* flow id*/, std::string>> flows;
std::map<std::size_t /* (x) */, DatasetMap> perFlowTputNormToExpectedRate, perFlowTputNormToActualRate,
                                            perFlowActualToExpectedRate, perFlowPacketLossRate;
std::map<std::size_t /* (x) */, DatasetMap> perFlowDroppedBySocket;
std::map<std::size_t /* (x) */, DatasetMap> perFlowMedianE2eLatency;
std::map<std::size_t /* (x) */, std::map<std::size_t /* flow id */, DatasetMap>> perFlowE2eLatencyEcdf;
DatasetMap medianDlMuCompleteness, medianHeTbCompleteness;
std::map<std::size_t /* (x) */, DatasetMap> perTestDlMuCompleteness, perTestHeTbCompleteness;
DatasetMap medianDlMuDuration, medianHeTbDuration;
std::map<std::size_t /* (x) */, DatasetMap> perTestDlMuDuration, perTestHeTbDuration;

std::map<std::pair<std::size_t /* (x) */, std::size_t /* (y) */>, std::vector<std::string>> perFlowStats;

void ReadOptions (std::istream& is)
{
  std::string line, token;

  while (std::getline (is, line))
    {
      // skip empty lines and lines beginning with '#'
      if (line.empty () || line[0] == '#')
        {
          continue;
        }
      std::stringstream inbuf (line);
      options.emplace_back ();  // new parameter

      // handle lines beginning with (x) or (y)
      if (line[0] == '(')
        {
          char c;
          inbuf >> c;  // consume '('
          NS_ABORT_MSG_IF (!(inbuf >> c) || (c != 'x' && c != 'y' && c != 'a'),
                           "Expected (x), (y) or (a) on line " << line);
          NS_ABORT_MSG_IF (c == 'x' && xParam != options.end (), "(x) can be specified just once");
          NS_ABORT_MSG_IF (c == 'y' && yParam != options.end (), "(y) can be specified just once");
          if (c == 'x')
            {
              xParam = std::prev (options.end ());
            }
          if (c == 'y')
            {
              yParam = std::prev (options.end ());
            }
          if (c == 'a')
            {
              aParams.push_back (std::prev (options.end ()));
            }
          NS_ABORT_MSG_IF (!(inbuf >> c) || c != ')', "Expected (x), (y) or (a) on line " << line);
        }

      while (inbuf >> token)
        {
          options.back ().push_back (token);
        }

      // at least two tokens are required
      if (options.back ().size () < 2)
        {
          NS_ABORT_MSG_IF (xParam == std::prev (options.end ()), "Lines starting with (x) must have at least a value");
          NS_ABORT_MSG_IF (yParam == std::prev (options.end ()), "Lines starting with (y) must have at least a value");

          std::cout << "Ignoring line (" << line << ")" << std::endl;
          options.pop_back ();
        }

      NS_ABORT_MSG_IF (options.back ().size () > 2
                       && xParam != std::prev (options.end ()) && yParam != std::prev (options.end ())
                       && std::find (aParams.begin (), aParams.end (), std::prev (options.end ())) == aParams.end (),
                       "Only lines starting with (x), (y) or (a) can have more than a value (" << line << ")");
    }
  // all the (a) options must have the same size as the (x) option
  for (auto& aParam : aParams)
    {
      NS_ABORT_MSG_IF (aParam->size () != xParam->size (),
                       "Option " << aParam->at (0) << " must have the same number of values as " << xParam->at (0));
    }
}

std::string PrintCmd (std::size_t x, std::size_t y)
{
  std::string cmd ("waf --run 'wifi-ofdma");
  std::size_t pos = confFileName.find_last_of ('/');

  for (auto optionIt = options.begin (); optionIt != options.end (); optionIt++)
    {
      std::string value = optionIt->at (1);
      if (optionIt == xParam)
        {
          value = optionIt->at (x);
        }
      else if (optionIt == yParam)
        {
          value = optionIt->at (y);
        }
      else
        {
          for (auto& aParam : aParams)
            {
              if (optionIt == aParam)
                {
                  value = optionIt->at (x);
                }
            }
        }

      // prepend path if the config file is not in the working dir
      if (optionIt->at (0) == "trafficFile" && pos != std::string::npos)
        {
          value = confFileName.substr (0, pos + 1) + value;
        }

      cmd.append (" --" + optionIt->at (0) + "=" + value);
    }
  cmd.append ("'");

  return cmd;
}


void ReadPerFlowStats (std::istream& is, std::size_t x, std::size_t y)
{
  std::string line;
  uint16_t f = 0;
  double tput = 0.0, expectedDataRate = 0.0;

  while (std::getline (is, line))
    {
      perFlowStats[{x, y}].push_back (line);

      if (line.empty ())
        {
          continue;
        }
      else if (line.substr (0, 4) == "FLOW")
        {
          f++;
          flows[x][f] = line;
        }
      else if (line.substr (0, 10) == "Throughput")
        {
          std::stringstream inbuf (line.substr (11));
          NS_ABORT_MSG_IF (!(inbuf >> tput), "Unable to read per-flow throughput");
        }
      else if (line.substr (0, 18) == "Expected data rate")
        {
          std::stringstream inbuf (line.substr (19));
          NS_ABORT_MSG_IF (!(inbuf >> expectedDataRate), "Unable to read expected data rate");
          if (expectedDataRate > 0.0)
            {
              perFlowTputNormToExpectedRate[x][y].Add (f, tput / expectedDataRate);
            }
        }
      else if (line.substr (0, 16) == "Actual data rate")
        {
          std::stringstream inbuf (line.substr (17));
          double dataRate;
          NS_ABORT_MSG_IF (!(inbuf >> dataRate), "Unable to read actual data rate");
          if (dataRate > 0.0)
            {
              perFlowTputNormToActualRate[x][y].Add (f, tput / dataRate);
              perFlowActualToExpectedRate[x][y].Add (f, dataRate / expectedDataRate);
            }
        }
      else if (line.substr (0, 16) == "Packet loss rate")
        {
          std::stringstream inbuf (line.substr (17));
          double lossRate;
          NS_ABORT_MSG_IF (!(inbuf >> lossRate), "Unable to read packet loss rate");
          perFlowPacketLossRate[x][y].Add (f, lossRate);
        }
      else if (line.substr (0, 27) == "Dropped packets (app layer)")
        {
          std::stringstream inbuf (line.substr (28));
          uint64_t dropped;
          NS_ABORT_MSG_IF (!(inbuf >> dropped), "Unable to read packets dropped by socket");
          perFlowDroppedBySocket[x][y].Add (f, dropped);
        }
      else if (line.substr (0, 7) == "Latency")
        {
          std::stringstream inbuf (line.substr (8));
          double latency;
          char c;
          std::vector<double> vec;
          NS_ABORT_MSG_IF (!(inbuf >> c), "'(' expected");
          while (inbuf >> latency)
            {
              vec.push_back (latency);
              inbuf >> c;
              if (c == ')')
                {
                  break;
                }
            }
          for (std::size_t i = 0; i < vec.size (); i++)
            {
              perFlowE2eLatencyEcdf[x][f][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
            }
          if (!vec.empty ())
            {
              perFlowMedianE2eLatency[x][y].Add (f, vec[(vec.size () - 1) / 2]);
            }
        }
      else if (line.substr (0, 15) == "PAIRWISE PER-AC")
        {
          break;
        }
    }
}


void ReadThroughputData (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DOWNLINK, UPLINK} dir = DOWNLINK;
  std::string line;
  double dlTotal = 0.0;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 5) == "TOTAL")
        {
          std::stringstream inbuf (line.substr (6));
          double total;
          NS_ABORT_MSG_IF (!(inbuf >> total), "Unable to read the total value");

          if (dir == DOWNLINK)
            {
              throughputDl[y].Add (x, total);
              dlTotal = total;
            }
          else
            {
              throughputUl[y].Add (x, total);
              throughput[y].Add (x, total + dlTotal);
              break;
            }
        }
      else if (line.substr (0, 10) == "Throughput")
        {
          dir = UPLINK;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");
          if (s == "Total:")
            {
              double acTotal;
              NS_ABORT_MSG_IF (!(inbuf >> acTotal), "Couldn't read total for AC " << ac);
              if (dir == DOWNLINK)
                {
                  perAcThroughputDl[ac][y].Add (x, acTotal);
                }
              else
                {
                  perAcThroughputUl[ac][y].Add (x, acTotal);
                }
            }
        }
    }
}


void ReadPacketLossData (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DOWNLINK, UPLINK} dir = DOWNLINK;
  enum {TRANSMITTED, RECEIVED} what = TRANSMITTED;
  std::string line;
  std::map<std::string /* AC */, uint64_t> txDl, txUl;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 5) == "TOTAL")
        {
          if (what == RECEIVED && dir == UPLINK)
            {
              break;
            }
        }
      else if (line.substr (0, 11) == "Transmitted")
        {
          dir = UPLINK;
        }
      else if (line.substr (0, 8) == "Received")
        {
          if (what == TRANSMITTED)
            {
              what = RECEIVED;
              dir = DOWNLINK;
            }
          else
            {
              dir = UPLINK;
            }
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");
          if (s == "Total:")
            {
              uint64_t acTotal;
              NS_ABORT_MSG_IF (!(inbuf >> acTotal), "Couldn't read total for AC " << ac);
              if (what == TRANSMITTED)
                {
                  if (dir == DOWNLINK)
                    {
                      txDl[ac] = acTotal;
                    }
                  else
                    {
                      txUl[ac] = acTotal;
                    }
                }
              else
                {
                  if (dir == DOWNLINK)
                    {
                      perAcLossRateDl[ac][y].Add (x, 1. - static_cast<double> (acTotal) / txDl[ac]);
                    }
                  else
                    {
                      perAcLossRateUl[ac][y].Add (x, 1. - static_cast<double> (acTotal) / txUl[ac]);
                    }
                }
            }
        }
    }
}


void ReadExpiredData (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DOWNLINK, UPLINK} dir = DOWNLINK;
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 5) == "TOTAL")
        {
          std::stringstream inbuf (line.substr (6));
          double total;
          NS_ABORT_MSG_IF (!(inbuf >> total), "Unable to read the total value");

          if (dir == DOWNLINK)
            {
              expiredDl[y].Add (x, total);
            }
          else
            {
              expiredUl[y].Add (x, total);
              break;
            }
        }
      else if (line.substr (0, 13) == "MSDUs expired")
        {
          dir = UPLINK;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");
          if (s == "Total:")
            {
              double acTotal;
              NS_ABORT_MSG_IF (!(inbuf >> acTotal), "Couldn't read total for AC " << ac);
              if (dir == DOWNLINK)
                {
                  perAcExpiredDl[ac][y].Add (x, acTotal);
                }
              else
                {
                  perAcExpiredUl[ac][y].Add (x, acTotal);
                }
            }
        }
    }
}


void ReadRejectedData (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DOWNLINK, UPLINK} dir = DOWNLINK;
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 5) == "TOTAL")
        {
          std::stringstream inbuf (line.substr (6));
          double total;
          NS_ABORT_MSG_IF (!(inbuf >> total), "Unable to read the total value");

          if (dir == DOWNLINK)
            {
              rejectedDl[y].Add (x, total);
            }
          else
            {
              rejectedUl[y].Add (x, total);
              break;
            }
        }
      else if (line.substr (0, 14) == "MSDUs rejected")
        {
          dir = UPLINK;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");
          if (s == "Total:")
            {
              double acTotal;
              NS_ABORT_MSG_IF (!(inbuf >> acTotal), "Couldn't read total for AC " << ac);
              if (dir == DOWNLINK)
                {
                  perAcRejectedDl[ac][y].Add (x, acTotal);
                }
              else
                {
                  perAcRejectedUl[ac][y].Add (x, acTotal);
                }
            }
        }
    }
}


void ReadTxFailuresData (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DOWNLINK, UPLINK} dir = DOWNLINK;
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 5) == "TOTAL")
        {
          std::stringstream inbuf (line.substr (6));
          double total;
          NS_ABORT_MSG_IF (!(inbuf >> total), "Unable to read the total value");

          if (dir == DOWNLINK)
            {
              txFailuresDl[y].Add (x, total);
            }
          else
            {
              txFailuresUl[y].Add (x, total);
              break;
            }
        }
      else if (line.substr (0, 11) == "TX failures")
        {
          dir = UPLINK;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");
          if (s == "Total:")
            {
              double acTotal;
              NS_ABORT_MSG_IF (!(inbuf >> acTotal), "Couldn't read total for AC " << ac);
              if (dir == DOWNLINK)
                {
                  perAcTxFailuresDl[ac][y].Add (x, acTotal);
                }
              else
                {
                  perAcTxFailuresUl[ac][y].Add (x, acTotal);
                }
            }
        }
    }
}


void ReadPerAcE2eLatencyData (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DOWNLINK, UPLINK} dir = DOWNLINK;
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 5) == "TOTAL")
        {
          if (dir == UPLINK)
            {
              break;
            }
        }
      else if (line.substr (0, 11) == "E2e latency")
        {
          dir = UPLINK;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");
          if (s == "Total:")
            {
              double latency;
              char c;
              std::vector<double> vec;
              NS_ABORT_MSG_IF (!(inbuf >> c), "'(' expected");
              
              while (inbuf >> latency)
                {
                  vec.push_back (latency);
                  inbuf >> c;
                  if (c == ')')
                    {
                      break;
                    }
                }

              if (dir == DOWNLINK)
                {
                  for (std::size_t i = 0; i < vec.size (); i++)
                    {
                      perAcE2eLatencyDlEcdf[ac][x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                    }
                  if (!vec.empty ())
                    {
                      perAcMedianE2eLatencyDl[ac][y].Add (x, vec[(vec.size () - 1) / 2]);
                    }
                }
              else
                {
                  for (std::size_t i = 0; i < vec.size (); i++)
                    {
                      perAcE2eLatencyUlEcdf[ac][x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                    }
                  if (!vec.empty ())
                    {
                      perAcMedianE2eLatencyUl[ac][y].Add (x, vec[(vec.size () - 1) / 2]);
                    }
                }
            }
        }
    }
}


void ReadQdiscSojournData (std::istream& is, std::size_t x, std::size_t y)
{
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty ())
        {
          break;
        }
      else if (line[0] == '-' || line.substr (0, 5) == "TOTAL")
        {
          continue;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");

          if (s == "TotalDL:")
            {
              double sojourn;
              char c;
              std::vector<double> vec;
              NS_ABORT_MSG_IF (!(inbuf >> c), "'(' expected");
              
              while (inbuf >> sojourn)
                {
                  vec.push_back (sojourn);
                  inbuf >> c;
                  if (c == ')')
                    {
                      break;
                    }
                }

              for (std::size_t i = 0; i < vec.size (); i++)
                {
                  perAcQdiscSojournDlEcdf[ac][x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                }
              if (!vec.empty ())
                {
                  perAcMedianQdiscSojournDl[ac][y].Add (x, vec[(vec.size () - 1) / 2]);
                }
            }
          else if (s == "TotalUL:")
            {
              double sojourn;
              char c;
              std::vector<double> vec;
              NS_ABORT_MSG_IF (!(inbuf >> c), "'(' expected");
              
              while (inbuf >> sojourn)
                {
                  vec.push_back (sojourn);
                  inbuf >> c;
                  if (c == ')')
                    {
                      break;
                    }
                }

              for (std::size_t i = 0; i < vec.size (); i++)
                {
                  perAcQdiscSojournUlEcdf[ac][x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                }
              if (!vec.empty ())
                {
                  perAcMedianQdiscSojournUl[ac][y].Add (x, vec[(vec.size () - 1) / 2]);
                }
            }
        }
    }
}


void ReadL2LatencyData (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DOWNLINK, UPLINK} dir = DOWNLINK;
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 5) == "TOTAL")
        {
          if (dir == UPLINK)
            {
              break;
            }
        }
      else if (line.substr (0, 10) == "L2 latency")
        {
          dir = UPLINK;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");
          if (s == "Total:")
            {
              double latency;
              char c;
              std::vector<double> vec;
              NS_ABORT_MSG_IF (!(inbuf >> c), "'(' expected");
              
              while (inbuf >> latency)
                {
                  vec.push_back (latency);
                  inbuf >> c;
                  if (c == ')')
                    {
                      break;
                    }
                }

              if (dir == DOWNLINK)
                {
                  for (std::size_t i = 0; i < vec.size (); i++)
                    {
                      perAcL2LatencyDlEcdf[ac][x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                    }
                  if (!vec.empty ())
                    {
                      perAcMedianL2LatencyDl[ac][y].Add (x, vec[(vec.size () - 1) / 2]);
                    }
                }
              else
                {
                  for (std::size_t i = 0; i < vec.size (); i++)
                    {
                      perAcL2LatencyUlEcdf[ac][x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                    }
                  if (!vec.empty ())
                    {
                      perAcMedianL2LatencyUl[ac][y].Add (x, vec[(vec.size () - 1) / 2]);
                    }
                }
            }
        }
    }
}


void ReadPairwiseHolData (std::istream& is, std::size_t x, std::size_t y)
{
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty () || line.substr (0, 5) == "TOTAL")
        {
          break;
        }
      else if (line[0] == '-')
        {
          continue;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");
          if (s == "Total:")
            {
              double latency;
              char c;
              std::vector<double> vec;
              NS_ABORT_MSG_IF (!(inbuf >> c), "'(' expected");
              
              while (inbuf >> latency)
                {
                  vec.push_back (latency);
                  inbuf >> c;
                  if (c == ')')
                    {
                      break;
                    }
                }

              for (std::size_t i = 0; i < vec.size (); i++)
                {
                  perAcPairwiseHolDlEcdf[ac][x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                }
              if (!vec.empty ())
                {
                  perAcMedianPairwiseHolDl[ac][y].Add (x, vec[(vec.size () - 1) / 2]);
                }
            }
        }
    }
}


void ReadAggregateHolData (std::istream& is, std::size_t x, std::size_t y)
{
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty ())
        {
          break;
        }
      else if (line[0] == '-')
        {
          continue;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");

          if (s == "AP:")
            {
              double latency;
              char c;
              std::vector<double> vec;
              NS_ABORT_MSG_IF (!(inbuf >> c), "'(' expected");
              
              while (inbuf >> latency)
                {
                  vec.push_back (latency);
                  inbuf >> c;
                  if (c == ')')
                    {
                      break;
                    }
                }

              for (std::size_t i = 0; i < vec.size (); i++)
                {
                  perAcAggregateHolDlEcdf[ac][x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                }
              if (!vec.empty ())
                {
                  perAcMedianAggregateHolDl[ac][y].Add (x, vec[(vec.size () - 1) / 2]);
                }
            }
        }
    }
}


void ReadDroppedByQdiscData (std::istream& is, std::size_t x, std::size_t y)
{
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty ())
        {
          break;
        }
      else if (line[0] == '-' || line.substr (0, 5) == "TOTAL")
        {
          continue;
        }
      else
        {
          NS_ABORT_MSG_IF (line[0] != '[', "Malformed output file (" << line << ")");
          std::string ac (line.substr (1, 2));
          std::stringstream inbuf (line.substr (5));
          std::string s;

          NS_ABORT_MSG_IF (!(inbuf >> s), "Nothing followed [" << ac << "]");

          if (s == "TotalDL:")
            {
              uint64_t dropped;
              NS_ABORT_MSG_IF (!(inbuf >> dropped), "count of dropped packets expected");
              perAcDroppedByQdiscDl[ac][y].Add (x, dropped);
            }
          else if (s == "TotalUL:")
            {
              uint64_t dropped;
              NS_ABORT_MSG_IF (!(inbuf >> dropped), "count of dropped packets expected");
              perAcDroppedByQdiscUl[ac][y].Add (x, dropped);
            }
        }
    }
}


void ReadCompleteness (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DLMU, HETB} ppdu = DLMU;
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 23) == "HE TB PPDU completeness")
        {
          ppdu = HETB;
        }
      else
        {
          double sample;
          char c;
          std::vector<double> vec;
          std::stringstream inbuf (line);
          NS_ABORT_MSG_IF (!(inbuf >> c) || (c != '('), "'(' expected");

          while (inbuf >> sample)
            {
              vec.push_back (sample);
              inbuf >> c;
              if (c == ')')
                {
                  break;
                }
            }

          for (std::size_t i = 0; i < vec.size (); i++)
            {
              if (ppdu == DLMU)
                {
                  perTestDlMuCompleteness[x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                }
              else
                {
                  perTestHeTbCompleteness[x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                }
            }
          if (!vec.empty ())
            {
              if (ppdu == DLMU)
                {
                  medianDlMuCompleteness[y].Add (x, vec[(vec.size () - 1) / 2]);
                }
              else
                {
                  medianHeTbCompleteness[y].Add (x, vec[(vec.size () - 1) / 2]);
                }
            }
          if (ppdu == HETB)
            {
              break;
            }
        }
    }
}


void ReadMuPpduDuration (std::istream& is, std::size_t x, std::size_t y)
{
  enum {DLMU, HETB} ppdu = DLMU;
  std::string line;

  while (std::getline (is, line))
    {
      if (line.empty () || line[0] == '-')
        {
          continue;
        }
      else if (line.substr (0, 19) == "HE TB PPDU duration")
        {
          ppdu = HETB;
        }
      else
        {
          double sample;
          char c;
          std::vector<double> vec;
          std::stringstream inbuf (line);
          NS_ABORT_MSG_IF (!(inbuf >> c) || (c != '('), "'(' expected");

          while (inbuf >> sample)
            {
              vec.push_back (sample);
              inbuf >> c;
              if (c == ')')
                {
                  break;
                }
            }

          for (std::size_t i = 0; i < vec.size (); i++)
            {
              if (ppdu == DLMU)
                {
                  perTestDlMuDuration[x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                }
              else
                {
                  perTestHeTbDuration[x][y].Add (vec[i], static_cast<double> (i) / (vec.size () - 1));
                }
            }
          if (!vec.empty ())
            {
              if (ppdu == DLMU)
                {
                  medianDlMuDuration[y].Add (x, vec[(vec.size () - 1) / 2]);
                }
              else
                {
                  medianHeTbDuration[y].Add (x, vec[(vec.size () - 1) / 2]);
                }
            }
          if (ppdu == HETB)
            {
              break;
            }
        }
    }
}


void ScanOutputFile (std::istream& is)
{
  std::string line, token;

  while (std::getline (is, line))
    {
      // search for a line starting with '#'
      if (line.empty () || line[0] != '#')
        {
          continue;
        }
      std::stringstream inbuf (line.substr (1));
      char c;
      std::size_t x, y;

      // read (x) and (y)
      NS_ABORT_MSG_IF (!(inbuf >> c) || c != '(', "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> c) || c != 'x', "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> c) || c != ')', "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> c) || c != '=', "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> x), "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> c) || c != '(', "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> c) || c != 'y', "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> c) || c != ')', "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> c) || c != '=', "Malformed line specifying (x) and (y)");
      NS_ABORT_MSG_IF (!(inbuf >> y), "Malformed line specifying (x) and (y)");

      NS_ABORT_MSG_IF (x >= xParam->size (), "(x) exceeds the number of parameter values");
      NS_ABORT_MSG_IF (y >= yParam->size (), "(y) exceeds the number of parameter values");

      // Search for statistics
      while (std::getline (is, line))
        {
          if (line.substr (0, 19) == "PER-FLOW statistics")
            {
              ReadPerFlowStats (is, x, y);
            }
          if (line.substr (0, 17) == "Throughput (Mbps)")
            {
              ReadThroughputData (is, x, y);
            }
          if (line.substr (0, 16) == "E2e latency (ms)")
            {
              ReadPerAcE2eLatencyData (is, x, y);
            }
          if (line.substr (0, 19) == "Transmitted packets")
            {
              ReadPacketLossData (is, x, y);
            }
          if (line.substr (0, 23) == "MSDUs expired at the AP")
            {
              ReadExpiredData (is, x, y);
            }
          if (line.substr (0, 24) == "MSDUs rejected at the AP")
            {
              ReadRejectedData (is, x, y);
            }
          if (line.substr (0, 21) == "TX failures at the AP")
            {
              ReadTxFailuresData (is, x, y);
            }
          if (line.substr (0, 10) == "L2 latency")
            {
              ReadL2LatencyData (is, x, y);
            }
          if (line.substr (0, 21) == "Pairwise Head-of-Line")
            {
              ReadPairwiseHolData (is, x, y);
            }
          if (line.substr (0, 22) == "Aggregate Head-of-Line")
            {
              ReadAggregateHolData (is, x, y);
            }
          if (line.substr (0, 30) == "Sojourn time in the queue disc")
            {
              ReadQdiscSojournData (is, x, y);
            }
          if (line.substr (0, 33) == "Packets dropped by the queue disc")
            {
              ReadDroppedByQdiscData (is, x, y);
            }
          if (line.substr (0, 23) == "DL MU PPDU completeness")
            {
              ReadCompleteness (is, x, y);
            }
          if (line.substr (0, 19) == "DL MU PPDU duration")
            {
              ReadMuPpduDuration (is, x, y);
              break;
            }
        }
    }
}


void Plot (std::string suffix, std::string yLabel, DatasetMap& data,
           std::string title = "", bool perFlow = false)
{
  Gnuplot plot (confFileName + "-" + suffix + ".eps");

  for (auto& dataset : data)
    {
      dataset.second.SetTitle (yParam->at (0) + "=" + yParam->at (dataset.first));
      dataset.second.SetStyle (Gnuplot2dDataset::LINES_POINTS);
      dataset.second.SetExtra ("ps 1.5 lw 5");
      plot.AddDataset (dataset.second);
    }
  plot.SetTerminal ("postscript eps color enh \"Times-BoldItalic\"");
  if (!title.empty ())
    {
      plot.SetTitle (title);
    }

  if (!perFlow)
    {
      plot.SetLegend (xParam->at (0), yLabel);

      std::stringstream xtics;
      std::size_t i = 1;
      for (auto it = std::next (xParam->begin ()); it != xParam->end (); it++)
        {
          xtics << "\"" << *it << "\" " << i++ << ", ";
        }
      plot.SetExtra ("set xtics (" + xtics.str () + ")");

      plot.AppendExtra ("set xrange [0.5:" + std::to_string (xParam->size () - 0.5) + "]");
    }
  else
    {
      plot.SetLegend ("Flows", yLabel);
    }

  std::string plotFilename (confFileName + "-" + suffix + ".plt");
  std::ofstream plotfile (plotFilename);
  plot.GenerateOutput (plotfile);
  plotfile.close ();

  int ret = system (std::string ("gnuplot " + plotFilename).c_str ());
  NS_ABORT_MSG_IF (ret != 0, "Error during system call (gnuplot " << plotFilename << ")");
}


void PlotEcdf (std::string suffix, std::string xLabel, DatasetMap& data, std::string title = "")
{
  Gnuplot plot (confFileName + "-" + suffix + ".eps");

  for (auto& dataset : data)
    {
      dataset.second.SetTitle (yParam->at (0) + "=" + yParam->at (dataset.first));
      dataset.second.SetStyle (Gnuplot2dDataset::LINES);
      dataset.second.SetExtra ("lw 5");
      plot.AddDataset (dataset.second);
    }
  plot.SetTerminal ("postscript eps color enh \"Times-BoldItalic\"");
  plot.SetLegend (xLabel, "ECDF");
  if (!title.empty ())
    {
      plot.SetTitle (title);
    }

  std::string plotFilename (confFileName + "-" + suffix + ".plt");
  std::ofstream plotfile (plotFilename);
  plot.GenerateOutput (plotfile);
  plotfile.close ();

  int ret = system (std::string ("gnuplot " + plotFilename).c_str ());
  NS_ABORT_MSG_IF (ret != 0, "Error during system call (gnuplot " << plotFilename << ")");
}

void PrintLatex (std::string filename)
{
  std::fstream fs;
  bool leftFig = true; // alternate figs on the left and on the right
  uint8_t row = 0;     // max three rows of figures per page

  fs.open (filename + "-report.tex", std::ios::out);

  NS_ABORT_MSG_IF (!fs.is_open (), "Could not open .tex file");

  fs << "\\documentclass{article}" << std::endl
     << "\\usepackage{verbatim}" << std::endl
     << "\\usepackage[a4paper, total={190mm,277mm}]{geometry}" << std::endl
     << "\\usepackage{listings}" << std::endl
     << "\\usepackage{graphicx}" << std::endl
     << "\\usepackage[caption=false,font=small,labelfont=sf,textfont=sf]{subfig}" << std::endl
     << "\\usepackage{hyperref}" << std::endl
     << "\\lstset{basicstyle=\\small\\ttfamily,columns=flexible,breaklines=true}" << std::endl
     << "\\begin{document}" << std::endl
     << "\\tableofcontents" << std::endl
     << "\\newpage" << std::endl
     << "\\section{Configuration file}" << std::endl
     << "\\verbatiminput{" << filename << "}" << std::endl
     << "\\newpage" << std::endl
     << "\\section{Per-flow statistics}" << std::endl;
    for (auto& test : perFlowStats)
      {
        fs << "\\begin{lstlisting}" << std::endl
           << "TEST: " << xParam->at (0) << " = " << xParam->at (test.first.first)
           << ", " << yParam->at (0) << " = " << yParam->at (test.first.second) << std::endl;
        for (auto& line : test.second)
          {
            fs << line << std::endl;
          }
        fs << "\\end{lstlisting}" << "\\clearpage";
      }
  fs << "\\section{Throughput for the BSS under test}" << std::endl
     << "Throughput is measured at the application end-points." << std::endl
     << "\\subsection{Overall Throughput}" << std::endl
     << "%" << std::endl
     << "\\begin{figure}[h]" << std::endl
     << "\\centering" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-throughput.eps}" << std::endl
     << "\\caption{Overall throughput (uplink + downlink, all ACs)}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\end{figure}" << std::endl
     << "%" << std::endl
     << "\\begin{figure}[h]" << std::endl
     << "\\centering" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-throughput-dl.eps}" << std::endl
     << "\\caption{Overall downlink throughput (all ACs)}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\hspace{1em}" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-throughput-ul.eps}" << std::endl
     << "\\caption{Overall uplink throughput (all ACs)}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\end{figure}" << std::endl
     << "\\clearpage" << std::endl
     << "\\subsection{Per-AC downlink throughput}" << std::endl;

  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcThroughputDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-throughput-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Overall downlink throughput for " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcThroughputDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }
  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC uplink throughput}" << std::endl;

  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcThroughputUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-throughput-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Overall uplink throughput for " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcThroughputUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-flow ratio of actual TX data rate to expected TX data rate}" << std::endl
     << "The actual TX data rate might differ from the expected TX data rate because the socket " << std::endl
     << "might discard some packets received by the application. This can only happen with TCP." << std::endl;
  leftFig = true;
  row = 0;
  for (auto& flowPlot : perFlowActualToExpectedRate)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-actual-expected-rate-flows-" << xParam->at (0) << "-" << xParam->at (flowPlot.first) << ".eps}" << std::endl
         << "\\caption{Per-flow ratio of actual TX data rate to expected TX data rate (" << xParam->at (0) << " = " << xParam->at (flowPlot.first) << ")}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && flowPlot.first != std::prev (perFlowActualToExpectedRate.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-flow throughput normalized to expected TX data rate}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& flowPlot : perFlowTputNormToExpectedRate)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-throughput-norm-expected-flows-" << xParam->at (0) << "-" << xParam->at (flowPlot.first) << ".eps}" << std::endl
         << "\\caption{Per-flow throughput normalized to expected TX data rate (" << xParam->at (0) << " = " << xParam->at (flowPlot.first) << ")}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && flowPlot.first != std::prev (perFlowTputNormToExpectedRate.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-flow throughput normalized to actual TX data rate}" << std::endl
     << "The actual TX data rate is determined by the packets that actually go through the transport layer socket."
     << std::endl;
  leftFig = true;
  row = 0;
  for (auto& flowPlot : perFlowTputNormToActualRate)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-throughput-norm-actual-flows-" << xParam->at (0) << "-" << xParam->at (flowPlot.first) << ".eps}" << std::endl
         << "\\caption{Per-flow throughput normalized to actual TX data rate (" << xParam->at (0) << " = " << xParam->at (flowPlot.first) << ")}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && flowPlot.first != std::prev (perFlowTputNormToActualRate.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\section{Dropped packets}" << std::endl
     << "The loss ratio for a given AC is computed as one minus the ratio of the total packets " << std::endl
     << "(of all the flows of the given AC) received by the receiver application to the total " << std::endl
     << "packets (of all the flows of the given AC) transmitted by the sender application. " << std::endl
     << "This metric makes little sense in case TCP is used." << std::endl
     << "\\subsection{Per-AC packet loss ratio (downlink)}" << std::endl;

  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcLossRateDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-loss-rate-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{DL Packet loss ratio for " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcLossRateDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC packet loss ratio (uplink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcLossRateUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-loss-rate-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{UL Packet loss ratio for " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcLossRateUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\section{Per-flow count of packets dropped by the socket}" << std::endl
     << "No packet is expected to be dropped by a UDP socket (they are immediately forwarded " << std::endl
     << "downstream). A TCP socket drops packets received when the send buffer is full " << std::endl
     << "(packets accumulate in the send buffer due to the TCP congestion control)." << std::endl;
  leftFig = true;
  row = 0;
  for (auto& flowPlot : perFlowDroppedBySocket)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-dropped-socket-flows-" << xParam->at (0) << "-" << xParam->at (flowPlot.first) << ".eps}" << std::endl
         << "\\caption{Per-flow count of packets dropped by the socket (" << xParam->at (0) << " = " << xParam->at (flowPlot.first) << ")}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && flowPlot.first != std::prev (perFlowDroppedBySocket.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\section{Packets dropped by queue discs}" << std::endl
     << "\\subsection{Per-AC count of dropped packets (downlink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcDroppedByQdiscDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-dropped-qdisc-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Number of packets of " << acPlot.first << " AC dropped by the qdisc at the AP}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcDroppedByQdiscDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC count of dropped packets (uplink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcDroppedByQdiscUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-dropped-qdisc-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Number of packets of " << acPlot.first << " AC dropped by the qdiscs at the STAs}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcDroppedByQdiscUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\section{Expired MSDUs for the BSS under test}" << std::endl
     << "\\begin{figure}[h]" << std::endl
     << "\\centering" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-expired-dl.eps}" << std::endl
     << "\\caption{Number of expired MSDUs (all ACs) for downlink}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\hspace{1em}" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-expired-ul.eps}" << std::endl
     << "\\caption{Number of expired MSDUs (all ACs) for uplink}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\end{figure}" << std::endl;

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC expired MSDUs (downlink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcExpiredDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-expired-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Number of expired MSDUs of " << acPlot.first << " AC for downlink}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcExpiredDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC expired MSDUs (uplink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcExpiredUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-expired-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Number of expired MSDUs of " << acPlot.first << " AC for uplink}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcExpiredUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }
  fs << "\\clearpage" << std::endl;

  fs << "\\section{MSDUs rejected (EDCA queue full) for the BSS under test}" << std::endl
     << "No packet should ever be dropped because the EDCA queue is full, due to the flow " << std::endl
     << "control implemented by the traffic control layer." << std::endl
     << "\\begin{figure}[h]" << std::endl
     << "\\centering" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-rejected-dl.eps}" << std::endl
     << "\\caption{Number of rejected MSDUs (all ACs) for downlink}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\hspace{1em}" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-rejected-ul.eps}" << std::endl
     << "\\caption{Number of rejected MSDUs (all ACs) for uplink}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\end{figure}" << std::endl
     << "\\clearpage" << std::endl;

  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcRejectedDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-rejected-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Number of rejected MSDUs of " << acPlot.first << " AC for downlink}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcRejectedDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }
  fs << "\\clearpage" << std::endl;

  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcRejectedUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-rejected-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Number of rejected MSDUs of " << acPlot.first << " AC for uplink}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcRejectedUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\section{TX failures for the BSS under test}" << std::endl
     << "Report the count of the MPDUs whose transmission was not acknowledged." << std::endl
     << "\\begin{figure}[h]" << std::endl
     << "\\centering" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-failures-dl.eps}" << std::endl
     << "\\caption{Number of TX failures (all ACs) for downlink}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\hspace{1em}" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-failures-ul.eps}" << std::endl
     << "\\caption{Number of TX failures (all ACs) for uplink}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\end{figure}" << std::endl;

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC TX failures (downlink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcTxFailuresDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-failures-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Number of TX failures of " << acPlot.first << " AC for downlink}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcTxFailuresDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC TX failures (uplink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcTxFailuresUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename << "-failures-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Number of TX failures of " << acPlot.first << " AC for uplink}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcTxFailuresUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }
  fs << "\\clearpage" << std::endl;

  fs << "\\section{End-to-end latency for the BSS under test}" << std::endl
     << "End-to-end latency is measured (per flow) at the application end-points." << std::endl
     << "\\subsection{Per-AC Median End-to-end latency (downlink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcMedianE2eLatencyDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename 
         << "-median-e2e-latency-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Median E2E latency for DL flows of " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcMedianE2eLatencyDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }
  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC Median End-to-end latency (uplink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcMedianE2eLatencyUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename 
         << "-median-e2e-latency-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Median E2E latency for UL flows of " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcMedianE2eLatencyUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{ECDF of End-to-end latency for each AC and for each test (downlink)}" << std::endl;
  for (auto& acMap : perAcE2eLatencyDlEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& acEcdf : acMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename 
             << "-e2e-latency-dl-ecdf-" << acMap.first << "-" << xParam->at (0) << "-" << xParam->at (acEcdf.first)
             << ".eps}" << std::endl
             << "\\caption{ECDF of E2E latency for DL flows of " << acMap.first << " AC (" << xParam->at (0)
             << " = " << xParam->at (acEcdf.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && acEcdf.first != std::prev (acMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\subsection{ECDF of End-to-end latency for each AC and for each test (uplink)}" << std::endl;
  for (auto& acMap : perAcE2eLatencyUlEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& acEcdf : acMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename 
             << "-e2e-latency-ul-ecdf-" << acMap.first << "-" << xParam->at (0) << "-" << xParam->at (acEcdf.first)
             << ".eps}" << std::endl
             << "\\caption{ECDF of E2E latency for UL flows of " << acMap.first << " AC (" << xParam->at (0)
             << " = " << xParam->at (acEcdf.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && acEcdf.first != std::prev (acMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\subsection{Median E2E latency for each flow and for each test}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& flowPlot : perFlowMedianE2eLatency)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-median-e2e-latency-flows-" << xParam->at (0) << "-" << xParam->at (flowPlot.first) << ".eps}" << std::endl
         << "\\caption{Median E2E latency (" << xParam->at (0) << " = " << xParam->at (flowPlot.first) << ")}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && flowPlot.first != std::prev (perFlowMedianE2eLatency.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{ECDF of E2E latency for each flow and for each test}" << std::endl;
  for (auto& xMap : perFlowE2eLatencyEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& flowEcdf : xMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename
             << "-e2e-latency-ecdf-" << xParam->at (0) << "-" << xParam->at (xMap.first) << "-flow-"
             << flowEcdf.first << ".eps}" << std::endl
             << "\\caption{ECDF of E2E latency of Flow " << flowEcdf.first << " (" << xParam->at (0)
             << " = " << xParam->at (xMap.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && flowEcdf.first != std::prev (xMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\section{Qdisc sojourn time for the BSS under test}" << std::endl
     << "\\subsection{Per-AC Median Qdisc sojourn time (downlink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcMedianQdiscSojournDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename 
         << "-median-qdisc-sojourn-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Median Qdisc sojourn time for DL flows of " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcMedianQdiscSojournDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }
  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC Median Qdisc sojourn time (uplink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcMedianQdiscSojournUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename 
         << "-median-qdisc-sojourn-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Median Qdisc sojourn time for UL flows of " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcMedianQdiscSojournUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{ECDF of Qdisc sojourn time for each AC and for each test (downlink)}" << std::endl;
  for (auto& acMap : perAcQdiscSojournDlEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& acEcdf : acMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename 
             << "-qdisc-sojourn-dl-ecdf-" << acMap.first << "-" << xParam->at (0) << "-" << xParam->at (acEcdf.first)
             << ".eps}" << std::endl
             << "\\caption{ECDF of Qdisc sojourn time for DL flows of " << acMap.first << " AC (" << xParam->at (0)
             << " = " << xParam->at (acEcdf.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && acEcdf.first != std::prev (acMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\subsection{ECDF of Qdisc sojourn time for each AC and for each test (uplink)}" << std::endl;
  for (auto& acMap : perAcQdiscSojournUlEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& acEcdf : acMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename 
             << "-qdisc-sojourn-ul-ecdf-" << acMap.first << "-" << xParam->at (0) << "-" << xParam->at (acEcdf.first)
             << ".eps}" << std::endl
             << "\\caption{ECDF of Qdisc sojourn time for UL flows of " << acMap.first << " AC (" << xParam->at (0)
             << " = " << xParam->at (acEcdf.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && acEcdf.first != std::prev (acMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\section{Layer-2 latency for the BSS under test}" << std::endl
     << "L2 latency is measured (per AC) as the interval from the time the transmitter's MAC receives"
     << " an MSDU to the time the receiver's MAC forwards the MSDU up." << std::endl
     << "\\subsection{Per-AC Layer-2 latency (downlink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcMedianL2LatencyDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename 
         << "-median-l2-latency-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Median L2 latency for DL flows of " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcMedianL2LatencyDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }
  fs << "\\clearpage" << std::endl
     << "\\subsection{Per-AC Layer-2 latency (uplink)}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcMedianL2LatencyUl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename 
         << "-median-l2-latency-ul-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Median L2 latency for UL flows of " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcMedianL2LatencyUl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{ECDF of Layer-2 latency for each AC and for each test (downlink)}" << std::endl;
  for (auto& acMap : perAcL2LatencyDlEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& acEcdf : acMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename 
             << "-l2-latency-dl-ecdf-" << acMap.first << "-" << xParam->at (0) << "-" << xParam->at (acEcdf.first)
             << ".eps}" << std::endl
             << "\\caption{ECDF of L2 latency for DL flows of " << acMap.first << " AC (" << xParam->at (0)
             << " = " << xParam->at (acEcdf.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && acEcdf.first != std::prev (acMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\subsection{ECDF of Layer-2 latency for each AC and for each test (uplink)}" << std::endl;
  for (auto& acMap : perAcL2LatencyUlEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& acEcdf : acMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename 
             << "-l2-latency-ul-ecdf-" << acMap.first << "-" << xParam->at (0) << "-" << xParam->at (acEcdf.first)
             << ".eps}" << std::endl
             << "\\caption{ECDF of L2 latency for UL flows of " << acMap.first << " AC (" << xParam->at (0)
             << " = " << xParam->at (acEcdf.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && acEcdf.first != std::prev (acMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\section{Head-of-Line delay for the BSS under test}" << std::endl
     << "Pairwise HoL delay is measured (per AC) as the interval between two consecutive A-MSDUs"
     << " dequeued from the EDCA queue and destined to a specific receiver." << std::endl
     << "Aggregate HoL delay is measured (per AC) as the interval between two consecutive A-MSDUs"
     << " dequeued from the EDCA queue and destined to any receiver." << std::endl
     << "\\subsection{Aggregate HoL delay}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcMedianAggregateHolDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename 
         << "-median-aggregate-hol-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Median Aggregate HoL delay at the AP for " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcMedianAggregateHolDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }
  fs << "\\clearpage" << std::endl;
  for (auto& acMap : perAcAggregateHolDlEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& acEcdf : acMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename 
             << "-aggregate-hol-dl-ecdf-" << acMap.first << "-" << xParam->at (0) << "-" << xParam->at (acEcdf.first)
             << ".eps}" << std::endl
             << "\\caption{ECDF of Aggregate HoL delay at the AP for " << acMap.first << " AC (" << xParam->at (0)
             << " = " << xParam->at (acEcdf.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && acEcdf.first != std::prev (acMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\subsection{Pairwise HoL delay}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& acPlot : perAcMedianPairwiseHolDl)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename 
         << "-median-pairwise-hol-dl-" << acPlot.first << ".eps}" << std::endl
         << "\\caption{Median Pairwise HoL delay at the AP for " << acPlot.first << " AC}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && acPlot.first != std::prev (perAcMedianPairwiseHolDl.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl;
  for (auto& acMap : perAcPairwiseHolDlEcdf)
    {
      leftFig = true;
      row = 0;
      for (auto& acEcdf : acMap.second)
        {
          if (leftFig)
            {
              fs << "\\begin{figure}[h]" << std::endl
                 << "\\centering" << std::endl;
            }
          fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
             << "\\centering" << std::endl
             << "\\includegraphics[width=\\textwidth]{" << filename 
             << "-pairwise-hol-dl-ecdf-" << acMap.first << "-" << xParam->at (0) << "-" << xParam->at (acEcdf.first)
             << ".eps}" << std::endl
             << "\\caption{ECDF of Pairwise HoL delay at the AP for " << acMap.first << " AC (" << xParam->at (0)
             << " = " << xParam->at (acEcdf.first) << ")}" << std::endl
             << "\\end{minipage}" << std::endl;
          if (leftFig && acEcdf.first != std::prev (acMap.second.end ())->first)
            {
              // figure on the left and not the last one
              fs << "\\hspace{1em}" << std::endl;
            }
          else
            {
              fs << "\\end{figure}" << std::endl
                 << "%" << std::endl;
              if (++row == 3)
                {
                  fs << "\\clearpage" << std::endl;
                  row = 0;
                }
            }
          leftFig = !leftFig;
        }
      fs << "\\clearpage" << std::endl;
    }

  fs << "\\section{Completeness of MU PPDU frames}" << std::endl
     << "\\begin{figure}[h]" << std::endl
     << "\\centering" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-median-dl-mu-completeness.eps}" << std::endl
     << "\\caption{Median of the DL MU PPDU completeness for each test}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\hspace{1em}" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-median-he-tb-completeness.eps}" << std::endl
     << "\\caption{Median of the HE TB PPDU completeness for each test}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\end{figure}" << std::endl
     << "\\clearpage" << std::endl
     << "\\subsection{ECDF of DL MU PPDU completeness for each test}" << std::endl;

  leftFig = true;
  row = 0;
  for (auto& xEcdf : perTestDlMuCompleteness)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-dl-mu-completeness-ecdf-" << xParam->at (0) << "-" + xParam->at (xEcdf.first) << ".eps}" << std::endl
         << "\\caption{ECDF of the DL MU Completeness for " << xParam->at (0) << " = " + xParam->at (xEcdf.first) << "}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && xEcdf.first != std::prev (perTestDlMuCompleteness.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{ECDF of HE TB PPDU completeness for each test}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& xEcdf : perTestHeTbCompleteness)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-he-tb-completeness-ecdf-" << xParam->at (0) << "-" + xParam->at (xEcdf.first) << ".eps}" << std::endl
         << "\\caption{ECDF of the HE TB Completeness for " << xParam->at (0) << " = " + xParam->at (xEcdf.first) << "}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && xEcdf.first != std::prev (perTestHeTbCompleteness.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\section{Duration of MU PPDU frames}" << std::endl
     << "\\begin{figure}[h]" << std::endl
     << "\\centering" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-median-dl-mu-duration.eps}" << std::endl
     << "\\caption{Median of the DL MU PPDU duration for each test}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\hspace{1em}" << std::endl
     << "\\begin{minipage}{.4\\textwidth}" << std::endl
     << "\\centering" << std::endl
     << "\\includegraphics[width=\\textwidth]{" << filename << "-median-he-tb-duration.eps}" << std::endl
     << "\\caption{Median of the HE TB PPDU duration for each test}" << std::endl
     << "\\end{minipage}" << std::endl
     << "\\end{figure}" << std::endl
     << "\\clearpage" << std::endl
     << "\\subsection{ECDF of DL MU PPDU duration for each test}" << std::endl;

  leftFig = true;
  row = 0;
  for (auto& xEcdf : perTestDlMuDuration)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-dl-mu-duration-ecdf-" << xParam->at (0) << "-" + xParam->at (xEcdf.first) << ".eps}" << std::endl
         << "\\caption{ECDF of the DL MU Duration for " << xParam->at (0) << " = " + xParam->at (xEcdf.first) << "}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && xEcdf.first != std::prev (perTestDlMuDuration.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\clearpage" << std::endl
     << "\\subsection{ECDF of HE TB PPDU duration for each test}" << std::endl;
  leftFig = true;
  row = 0;
  for (auto& xEcdf : perTestHeTbDuration)
    {
      if (leftFig)
        {
          fs << "\\begin{figure}[h]" << std::endl
              << "\\centering" << std::endl;
        }
      fs << "\\begin{minipage}{.4\\textwidth}" << std::endl
         << "\\centering" << std::endl
         << "\\includegraphics[width=\\textwidth]{" << filename
         << "-he-tb-duration-ecdf-" << xParam->at (0) << "-" + xParam->at (xEcdf.first) << ".eps}" << std::endl
         << "\\caption{ECDF of the HE TB Duration for " << xParam->at (0) << " = " + xParam->at (xEcdf.first) << "}" << std::endl
         << "\\end{minipage}" << std::endl;
      if (leftFig && xEcdf.first != std::prev (perTestHeTbDuration.end ())->first)
        {
          // figure on the left and not the last one
          fs << "\\hspace{1em}" << std::endl;
        }
      else
        {
          fs << "\\end{figure}" << std::endl
              << "%" << std::endl;
          if (++row == 3)
            {
              fs << "\\clearpage" << std::endl;
              row = 0;
            }
        }
      leftFig = !leftFig;
    }

  fs << "\\end{document}" << std::endl;

  fs.close ();

  // run pdflatex twice to get a correct table of contents
  int ret = 0;
  ret = system (std::string ("pdflatex " + filename + "-report.tex").c_str ());
  NS_ABORT_MSG_IF (ret != 0, "Error during system call (pdflatex " << filename << "-report.tex), 1st call");
  ret = system (std::string ("pdflatex " + filename + "-report.tex").c_str ());
  NS_ABORT_MSG_IF (ret != 0, "Error during system call (pdflatex " << filename << "-report.tex), 2nd call");

  std::size_t pos = filename.find_last_of ('/');
  if (pos != std::string::npos)
    {
      // the config file is in a dir other than the current working dir
      ret = system (std::string ("mv " + filename.substr (pos + 1) + "-report.pdf " + filename.substr (0, pos)).c_str ());
      NS_ABORT_MSG_IF (ret != 0, "Error during system call (mv " << filename.substr (pos + 1) << "-report.pdf " << filename.substr (0, pos) << ")");
    }

  ret = system (std::string ("rm " + filename.substr (pos + 1) + "-report.aux").c_str ());
  NS_ABORT_MSG_IF (ret != 0, "Error during system call (rm " << filename.substr (pos + 1) << "-report.aux)");
  ret = system (std::string ("rm " + filename.substr (pos + 1) + "-report.log").c_str ());
  NS_ABORT_MSG_IF (ret != 0, "Error during system call (rm " << filename.substr (pos + 1) << "-report.log)");
  ret = system (std::string ("rm " + filename.substr (pos + 1) + "-report.out").c_str ());
  NS_ABORT_MSG_IF (ret != 0, "Error during system call (rm " << filename.substr (pos + 1) << "-report.out)");
  ret = system (std::string ("rm " + filename.substr (pos + 1) + "-report.toc").c_str ());
  NS_ABORT_MSG_IF (ret != 0, "Error during system call (rm " << filename.substr (pos + 1) << "-report.toc)");
}

int 
main (int argc, char *argv[])
{
  std::fstream fs;
  bool noExec = false;

  CommandLine cmd;
  cmd.AddValue ("file", "Name of the config file", confFileName);
  cmd.AddValue ("noExec", "Do not run wifi-ofdma (plots only)", noExec);
  cmd.Parse (argc, argv);

  NS_ABORT_MSG_IF (confFileName.empty (), "file parameter is mandatory");

  fs.open (confFileName);

  NS_ABORT_MSG_IF (!fs.is_open (), "Cannot open the config file (" << confFileName << ")");

  ReadOptions (fs);
  fs.close ();

  if (!noExec)
    {
      int ret;
      // remove output file if present
      std::fstream testFs (std::string (confFileName + ".output").c_str (), std::fstream::in);
      if (testFs.is_open ())
        {
          testFs.close ();
          ret = system (std::string ("rm " + confFileName + ".output\n").c_str ());
          NS_ABORT_MSG_IF (ret != 0, "Error during system call (rm " << confFileName << ".output)");
        }

      std::size_t yCount = (yParam != options.end () ? yParam->size () : 2);
      std::size_t xCount = (xParam != options.end () ? xParam->size () : 2);

      for (std::size_t y = 1; y < yCount; y++)
        {
          for (std::size_t x = 1; x < xCount; x++)
            {
              std::stringstream ss; 
              ss << "echo '# (x)=" << x << " (y)=" << y << "' >> " << confFileName << ".output";
              ret = system (ss.str ().c_str ());
              NS_ABORT_MSG_IF (ret != 0, "Error during system call (" << ss.str () << ")");

              std::string command = PrintCmd (x, y);
              // add redirection to output file
              command.append (" >> " + confFileName + ".output\n");
              // print command to execute
              std::cout << command << std::endl;
              // execute command
              ret = system (command.c_str ());
              NS_ABORT_MSG_IF (ret != 0, "Error during system call (" << command << ")");
            }
        }
    }

  if (xParam == options.end () || yParam == options.end ())
    {
      std::cout << "Missing (x) and (y) in the config file, no plot will be made" << std::endl;
      return 0;
    }

  fs.open (confFileName + ".output");

  NS_ABORT_MSG_IF (!fs.is_open (), "Cannot open the output file (" << confFileName << ".output)");

  ScanOutputFile (fs);

  Plot ("throughput-dl", "DL Throughput (Mbps)", throughputDl);
  Plot ("throughput-ul", "UL Throughput (Mbps)", throughputUl);
  Plot ("throughput", "Throughput (Mbps)", throughput);

  for (auto& acPlot : perAcThroughputDl)
    {
      Plot ("throughput-dl-" + acPlot.first, acPlot.first + " DL Throughput (Mbps)", acPlot.second);
    }
  for (auto& acPlot : perAcThroughputUl)
    {
      Plot ("throughput-ul-" + acPlot.first, acPlot.first + " UL Throughput (Mbps)", acPlot.second);
    }

  for (auto& acPlot : perAcLossRateDl)
    {
      Plot ("loss-rate-dl-" + acPlot.first, acPlot.first + " DL Loss rate", acPlot.second);
    }
  for (auto& acPlot : perAcLossRateUl)
    {
      Plot ("loss-rate-ul-" + acPlot.first, acPlot.first + " UL Loss rate", acPlot.second);
    }

  Plot ("expired-dl", "DL Expired MSDUs", expiredDl);
  Plot ("expired-ul", "UL Expired MSDUs", expiredUl);

  for (auto& acPlot : perAcExpiredDl)
    {
      Plot ("expired-dl-" + acPlot.first, acPlot.first + " DL Expired MSDUs", acPlot.second);
    }
  for (auto& acPlot : perAcExpiredUl)
    {
      Plot ("expired-ul-" + acPlot.first, acPlot.first + " UL Expired MSDUs", acPlot.second);
    }

  Plot ("rejected-dl", "DL Rejected MSDUs", rejectedDl);
  Plot ("rejected-ul", "UL Rejected MSDUs", rejectedUl);

  for (auto& acPlot : perAcRejectedDl)
    {
      Plot ("rejected-dl-" + acPlot.first, acPlot.first + " DL Rejected MSDUs", acPlot.second);
    }
  for (auto& acPlot : perAcRejectedUl)
    {
      Plot ("rejected-ul-" + acPlot.first, acPlot.first + " UL Rejected MSDUs", acPlot.second);
    }

  Plot ("failures-dl", "DL TX failures", txFailuresDl);
  Plot ("failures-ul", "UL TX failures", txFailuresUl);

  for (auto& acPlot : perAcTxFailuresDl)
    {
      Plot ("failures-dl-" + acPlot.first, acPlot.first + " DL TX failures", acPlot.second);
    }
  for (auto& acPlot : perAcTxFailuresUl)
    {
      Plot ("failures-ul-" + acPlot.first, acPlot.first + " UL TX failures", acPlot.second);
    }

  for (auto& acPlot : perAcMedianE2eLatencyDl)
    {
      Plot ("median-e2e-latency-dl-" + acPlot.first, acPlot.first + " DL Median E2E Latency (ms)",
            acPlot.second);
    }
  for (auto& acPlot : perAcMedianE2eLatencyUl)
    {
      Plot ("median-e2e-latency-ul-" + acPlot.first, acPlot.first + " UL Median E2E Latency (ms)",
            acPlot.second);
    }

  for (auto& acMap : perAcE2eLatencyDlEcdf)
    {
      for (auto& acEcdf : acMap.second)
        {
          PlotEcdf ("e2e-latency-dl-ecdf-" + acMap.first + "-" + xParam->at (0) + "-" + xParam->at (acEcdf.first),
                    "DL E2E Latency (ms)", acEcdf.second, acMap.first + ", " + xParam->at (0) + "=" + xParam->at (acEcdf.first));
        }
    }

  for (auto& acMap : perAcE2eLatencyUlEcdf)
    {
      for (auto& acEcdf : acMap.second)
        {
          PlotEcdf ("e2e-latency-ul-ecdf-" + acMap.first + "-" + xParam->at (0) + "-" + xParam->at (acEcdf.first),
                    "UL E2E Latency (ms)", acEcdf.second, acMap.first + ", " + xParam->at (0) + "=" + xParam->at (acEcdf.first));
        }
    }

  for (auto& acPlot : perAcMedianQdiscSojournDl)
    {
      Plot ("median-qdisc-sojourn-dl-" + acPlot.first, acPlot.first + " DL Median Qdisc Sojourn time (ms)",
            acPlot.second);
    }
  for (auto& acPlot : perAcMedianQdiscSojournUl)
    {
      Plot ("median-qdisc-sojourn-ul-" + acPlot.first, acPlot.first + " UL Median Qdisc Sojourn time (ms)",
            acPlot.second);
    }

  for (auto& acMap : perAcQdiscSojournDlEcdf)
    {
      for (auto& acEcdf : acMap.second)
        {
          PlotEcdf ("qdisc-sojourn-dl-ecdf-" + acMap.first + "-" + xParam->at (0) + "-" + xParam->at (acEcdf.first),
                    "DL Qdisc Sojourn time (ms)", acEcdf.second, acMap.first + ", " + xParam->at (0) + "=" + xParam->at (acEcdf.first));
        }
    }

  for (auto& acMap : perAcQdiscSojournUlEcdf)
    {
      for (auto& acEcdf : acMap.second)
        {
          PlotEcdf ("qdisc-sojourn-ul-ecdf-" + acMap.first + "-" + xParam->at (0) + "-" + xParam->at (acEcdf.first),
                    "UL Qdisc Sojourn time (ms)", acEcdf.second, acMap.first + ", " + xParam->at (0) + "=" + xParam->at (acEcdf.first));
        }
    }

  for (auto& acPlot : perAcMedianL2LatencyDl)
    {
      Plot ("median-l2-latency-dl-" + acPlot.first, acPlot.first + " DL Median L2 Latency (ms)",
            acPlot.second);
    }
  for (auto& acPlot : perAcMedianL2LatencyUl)
    {
      Plot ("median-l2-latency-ul-" + acPlot.first, acPlot.first + " UL Median L2 Latency (ms)",
            acPlot.second);
    }

  for (auto& acMap : perAcL2LatencyDlEcdf)
    {
      for (auto& acEcdf : acMap.second)
        {
          PlotEcdf ("l2-latency-dl-ecdf-" + acMap.first + "-" + xParam->at (0) + "-" + xParam->at (acEcdf.first),
                    "DL L2 Latency (ms)", acEcdf.second, acMap.first + ", " + xParam->at (0) + "=" + xParam->at (acEcdf.first));
        }
    }

  for (auto& acMap : perAcL2LatencyUlEcdf)
    {
      for (auto& acEcdf : acMap.second)
        {
          PlotEcdf ("l2-latency-ul-ecdf-" + acMap.first + "-" + xParam->at (0) + "-" + xParam->at (acEcdf.first),
                    "UL L2 Latency (ms)", acEcdf.second, acMap.first + ", " + xParam->at (0) + "=" + xParam->at (acEcdf.first));
        }
    }

  for (auto& acPlot : perAcMedianAggregateHolDl)
    {
      Plot ("median-aggregate-hol-dl-" + acPlot.first, acPlot.first + " DL Median Aggregate HoL Delay (ms)",
            acPlot.second);
    }

  for (auto& acMap : perAcAggregateHolDlEcdf)
    {
      for (auto& acEcdf : acMap.second)
        {
          PlotEcdf ("aggregate-hol-dl-ecdf-" + acMap.first + "-" + xParam->at (0) + "-" + xParam->at (acEcdf.first),
                    "DL Aggregate HoL Delay (ms)", acEcdf.second, acMap.first + ", " + xParam->at (0) + "=" + xParam->at (acEcdf.first));
        }
    }

  for (auto& acPlot : perAcMedianPairwiseHolDl)
    {
      Plot ("median-pairwise-hol-dl-" + acPlot.first, acPlot.first + " DL Median Pairwise HoL Delay (ms)",
            acPlot.second);
    }

  for (auto& acMap : perAcPairwiseHolDlEcdf)
    {
      for (auto& acEcdf : acMap.second)
        {
          PlotEcdf ("pairwise-hol-dl-ecdf-" + acMap.first + "-" + xParam->at (0) + "-" + xParam->at (acEcdf.first),
                    "DL Pairwise HoL Delay (ms)", acEcdf.second, acMap.first + ", " + xParam->at (0) + "=" + xParam->at (acEcdf.first));
        }
    }

  for (auto& acPlot : perAcDroppedByQdiscDl)
    {
      Plot ("dropped-qdisc-dl-" + acPlot.first, acPlot.first + " DL Count of packets dropped by queue disc",
            acPlot.second);
    }

  for (auto& acPlot : perAcDroppedByQdiscUl)
    {
      Plot ("dropped-qdisc-ul-" + acPlot.first, acPlot.first + " UL Count of packets dropped by queue discs",
            acPlot.second);
    }

  Plot ("median-dl-mu-completeness", "Median of the DL MU PPDU completeness", medianDlMuCompleteness);
  Plot ("median-he-tb-completeness", "Median of the HE TB PPDU completeness", medianHeTbCompleteness);

  for (auto& xEcdf : perTestDlMuCompleteness)
    {
      PlotEcdf ("dl-mu-completeness-ecdf-" + xParam->at (0) + "-" + xParam->at (xEcdf.first),
                "DL MU Completeness", xEcdf.second, xParam->at (0) + "=" + xParam->at (xEcdf.first));
    }

  for (auto& xEcdf : perTestHeTbCompleteness)
    {
      PlotEcdf ("he-tb-completeness-ecdf-" + xParam->at (0) + "-" + xParam->at (xEcdf.first),
                "HE TB Completeness", xEcdf.second, xParam->at (0) + "=" + xParam->at (xEcdf.first));
    }

  Plot ("median-dl-mu-duration", "Median of the DL MU PPDU duration", medianDlMuDuration);
  Plot ("median-he-tb-duration", "Median of the HE TB PPDU duration", medianHeTbDuration);

  for (auto& xEcdf : perTestDlMuDuration)
    {
      PlotEcdf ("dl-mu-duration-ecdf-" + xParam->at (0) + "-" + xParam->at (xEcdf.first),
                "DL MU Duration", xEcdf.second, xParam->at (0) + "=" + xParam->at (xEcdf.first));
    }

  for (auto& xEcdf : perTestHeTbDuration)
    {
      PlotEcdf ("he-tb-duration-ecdf-" + xParam->at (0) + "-" + xParam->at (xEcdf.first),
                "HE TB Duration", xEcdf.second, xParam->at (0) + "=" + xParam->at (xEcdf.first));
    }

  for (auto& flowPlot : perFlowActualToExpectedRate)
    {
      Plot ("actual-expected-rate-flows-" + xParam->at (0) + "-" + xParam->at (flowPlot.first),
            "Ratio of actual data rate to expected data rate",
            flowPlot.second, xParam->at (0) + " = " + xParam->at (flowPlot.first), true);
    }

  for (auto& flowPlot : perFlowTputNormToExpectedRate)
    {
      Plot ("throughput-norm-expected-flows-" + xParam->at (0) + "-" + xParam->at (flowPlot.first),
            "Throughput normalized to expected data rate",
            flowPlot.second, xParam->at (0) + " = " + xParam->at (flowPlot.first), true);
    }

  for (auto& flowPlot : perFlowTputNormToActualRate)
    {
      Plot ("throughput-norm-actual-flows-" + xParam->at (0) + "-" + xParam->at (flowPlot.first),
            "Throughput normalized to actual data rate",
            flowPlot.second, xParam->at (0) + " = " + xParam->at (flowPlot.first), true);
    }

  for (auto& flowPlot : perFlowDroppedBySocket)
    {
      Plot ("dropped-socket-flows-" + xParam->at (0) + "-" + xParam->at (flowPlot.first),
            "Count of packets dropped by the socket",
            flowPlot.second, xParam->at (0) + " = " + xParam->at (flowPlot.first), true);
    }

  for (auto& flowPlot : perFlowMedianE2eLatency)
    {
      Plot ("median-e2e-latency-flows-" + xParam->at (0) + "-" + xParam->at (flowPlot.first),
            "Median E2E Latency (ms)",
            flowPlot.second, xParam->at (0) + " = " + xParam->at (flowPlot.first), true);
    }

  for (auto& xMap : perFlowE2eLatencyEcdf)
    {
      for (auto& flowEcdf : xMap.second)
        {
          PlotEcdf ("e2e-latency-ecdf-" + xParam->at (0) + "-" + xParam->at (xMap.first) + "-flow-" + std::to_string (flowEcdf.first),
                    "E2E Latency (ms)", flowEcdf.second, flows[xMap.first][flowEcdf.first] + ", " + xParam->at (0) + "=" + xParam->at (xMap.first));
        }
    }

  fs.close ();

  PrintLatex (confFileName);
}
