/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 Universita' degli Studi di Napoli Federico II
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Stefano Avallone <stavallo@unina.it>
 */

#include "ns3/log.h"
#include "wifi-default-protection-manager.h"
#include "wifi-tx-parameters.h"
#include "wifi-mac-queue-item.h"
#include "ap-wifi-mac.h"


namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("WifiDefaultProtectionManager");

NS_OBJECT_ENSURE_REGISTERED (WifiDefaultProtectionManager);

TypeId
WifiDefaultProtectionManager::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::WifiDefaultProtectionManager")
    .SetParent<WifiProtectionManager> ()
    .SetGroupName ("Wifi")
    .AddConstructor<WifiDefaultProtectionManager> ()
    .AddAttribute ("EnableMuRts",
                   "If enabled, always protect a DL MU PPDU by sending an MU-RTS.",
                   BooleanValue (false),
                   MakeBooleanAccessor (&WifiDefaultProtectionManager::m_sendMuRts),
                   MakeBooleanChecker ())
  ;
  return tid;
}

WifiDefaultProtectionManager::WifiDefaultProtectionManager ()
{
  NS_LOG_FUNCTION (this);
}

WifiDefaultProtectionManager::~WifiDefaultProtectionManager ()
{
  NS_LOG_FUNCTION_NOARGS ();
}

std::unique_ptr<WifiProtection>
WifiDefaultProtectionManager::TryAddMpdu (Ptr<const WifiMacQueueItem> mpdu,
                                          const WifiTxParameters& txParams)
{
  NS_LOG_FUNCTION (this << *mpdu << &txParams);

  // If the TXVECTOR indicates a DL MU PPDU, call a separate method
  if (txParams.m_txVector.IsDlMu ())
    {
      return TryAddMpduToMuPpdu (mpdu, txParams);
    }

  // No protection for TB PPDUs
  if (txParams.m_txVector.IsUlMu ())
    {
      if (txParams.m_protection)
        {
          NS_ASSERT (txParams.m_protection->method == WifiProtection::NONE);
          return nullptr;
        }
      return std::unique_ptr<WifiProtection> (new WifiNoProtection);
    }

  // if this is a Trigger Frame, call a separate method
  if (mpdu->GetHeader ().IsTrigger ())
    {
      return TryUlMuTransmission (mpdu, txParams);
    }

  // if the current protection method (if any) is already RTS/CTS or CTS-to-Self,
  // it will not change by adding an MPDU
  if (txParams.m_protection
      && (txParams.m_protection->method == WifiProtection::RTS_CTS
          || txParams.m_protection->method == WifiProtection::CTS_TO_SELF))
    {
      return nullptr;
    }

  // if a protection method is set, it must be NONE
  NS_ASSERT (!txParams.m_protection || txParams.m_protection->method == WifiProtection::NONE);

  std::unique_ptr<WifiProtection> protection;
  protection = GetPsduProtection (mpdu->GetHeader (), txParams.GetSizeIfAddMpdu (mpdu),
                                  txParams.m_txVector);

  // return the newly computed method if none was set or it is not NONE
  if (!txParams.m_protection || protection->method != WifiProtection::NONE)
    {
      return protection;
    }
  // the protection method has not changed
  return nullptr;
}

std::unique_ptr<WifiProtection>
WifiDefaultProtectionManager::TryAggregateMsdu (Ptr<const WifiMacQueueItem> msdu,
                                                const WifiTxParameters& txParams)
{
  NS_LOG_FUNCTION (this << *msdu << &txParams);

  // if the current protection method is already RTS/CTS or CTS-to-Self,
  // it will not change by aggregating an MSDU
  NS_ASSERT (txParams.m_protection);
  if (txParams.m_protection->method == WifiProtection::RTS_CTS
      || txParams.m_protection->method == WifiProtection::CTS_TO_SELF)
    {
      return nullptr;
    }

  // If the TXVECTOR indicates a DL MU PPDU, the protection method will
  // not change by aggregating an MSDU
  if (txParams.m_txVector.IsDlMu ())
    {
      return nullptr;
    }

  // No protection for TB PPDUs
  if (txParams.m_txVector.IsUlMu ())
    {
      NS_ASSERT (txParams.m_protection->method == WifiProtection::NONE);
      return nullptr;
    }

  NS_ASSERT (txParams.m_protection->method == WifiProtection::NONE);

  std::unique_ptr<WifiProtection> protection;
  protection = GetPsduProtection (msdu->GetHeader (), txParams.GetSizeIfAggregateMsdu (msdu).second,
                                  txParams.m_txVector);

  // the protection method may still be none
  if (protection->method == WifiProtection::NONE)
    {
      return nullptr;
    }

  // the protection method has changed
  return protection;
}

std::unique_ptr<WifiProtection>
WifiDefaultProtectionManager::GetPsduProtection (const WifiMacHeader& hdr, uint32_t size,
                                                 const WifiTxVector& txVector) const
{
  NS_LOG_FUNCTION (this << hdr << size << txVector);

  // a non-initial fragment does not need to be protected, unless it is being retransmitted
  if (hdr.GetFragmentNumber () > 0 && !hdr.IsRetry ())
    {
      return std::unique_ptr<WifiProtection> (new WifiNoProtection ());
    }

  // check if RTS/CTS is needed
  if (m_mac->GetWifiRemoteStationManager ()->NeedRts (hdr, size))
    {
      WifiRtsCtsProtection* protection = new WifiRtsCtsProtection;
      protection->rtsTxVector = m_mac->GetWifiRemoteStationManager ()->GetRtsTxVector (hdr.GetAddr1 ());
      protection->ctsTxVector = m_mac->GetWifiRemoteStationManager ()->GetCtsTxVector (hdr.GetAddr1 (),
                                                                                       protection->rtsTxVector.GetMode ());
      return std::unique_ptr<WifiProtection> (protection);
    }

  // check if CTS-to-Self is needed
  if (m_mac->GetWifiRemoteStationManager ()->GetUseNonErpProtection ()
      && m_mac->GetWifiRemoteStationManager ()->NeedCtsToSelf (txVector))
    {
      WifiCtsToSelfProtection* protection = new WifiCtsToSelfProtection;
      protection->ctsTxVector = m_mac->GetWifiRemoteStationManager ()->GetCtsToSelfTxVector ();
      return std::unique_ptr<WifiProtection> (protection);
    }

  return std::unique_ptr<WifiProtection> (new WifiNoProtection ());
}

std::unique_ptr<WifiProtection>
WifiDefaultProtectionManager::TryAddMpduToMuPpdu (Ptr<const WifiMacQueueItem> mpdu,
                                                  const WifiTxParameters& txParams)
{
  NS_LOG_FUNCTION (this << *mpdu << &txParams);
  NS_ASSERT (txParams.m_txVector.IsDlMu ());

  if (!m_sendMuRts)
    {
      // No protection because sending MU-RTS is disabled
      if (!txParams.m_protection)
        {
          // a protection method has not been set yet
          return std::unique_ptr<WifiProtection> (new WifiNoProtection ());
        }
      NS_ASSERT (txParams.m_protection->method == WifiProtection::NONE);
      return nullptr;
    }

  NS_ASSERT (!txParams.m_protection
             || txParams.m_protection->method == WifiProtection::MU_RTS_CTS);

  WifiMuRtsCtsProtection* protection = nullptr;
  if (txParams.m_protection)
    {
      protection = static_cast<WifiMuRtsCtsProtection*> (txParams.m_protection.get ());
    }

  const WifiMacHeader& hdr = mpdu->GetHeader ();
  Mac48Address receiver = hdr.GetAddr1 ();

  if (txParams.GetPsduInfo (receiver) == nullptr)
    {
      // we get here if this is the first MPDU for this receiver.
      Ptr<ApWifiMac> apMac = DynamicCast<ApWifiMac> (m_mac);
      NS_ABORT_MSG_IF (apMac == 0, "HE APs only can send DL MU PPDUs");
      uint16_t staId = apMac->GetAssociationId (receiver);
      
      if (protection != nullptr)
        {
          // txParams.m_protection points to an existing WifiMuRtsCtsProtection object.
          // We have to return a copy of this object including the needed changes
          protection = new WifiMuRtsCtsProtection (*protection);
        }
      else
        {
          // we have to create a new WifiMuRtsCtsProtection object
          protection = new WifiMuRtsCtsProtection;

          // compute the TXVECTOR to use to send the MU-RTS Trigger Frame
          protection->muRtsTxVector = m_mac->GetWifiRemoteStationManager ()->GetRtsTxVector (receiver);
          // initialize the MU-RTS Trigger Frame
          // The Length, GI And LTF Type, MU-MIMO LTF Mode, Number Of HE-LTF Symbols, STBC,
          // LDPC Extra Symbol Segment, AP TX Power, Packet Extension, Spatial Reuse, Doppler
          // and UL HE-SIG-A2 Reserved subfields in the Common Info field are reserved.
          // (Sec. 9.3.1.23.4 of 802.11ax D3.0)
          protection->muRts.SetType (MU_RTS_TRIGGER);
          protection->muRts.SetUlBandwidth (txParams.m_txVector.GetChannelWidth ());
        }

      // Add a User Info field for the new receiver
      // The MCS, Coding Type, DCM, SS Allocation and Target RSSI fields in the User Info
      // field are reserved (Sec. 9.3.1.23.4 of 802.11ax D3.0)
      CtrlTriggerUserInfoField& ui = protection->muRts.AddUserInfoField ();
      ui.SetAid12 (staId);
      // TODO Improve the way RU allocation is set. For the moment, it's the first 20 MHz channel
      ui.SetRuAllocation (HeRu::RuSpec {true, HeRu::RU_242_TONE, 1});
      return std::unique_ptr<WifiMuRtsCtsProtection> (protection);
    }

  // an MPDU addressed to the same receiver has been already added
  NS_ASSERT (protection != nullptr);

  // no change is needed
  return nullptr;
}

std::unique_ptr<WifiProtection>
WifiDefaultProtectionManager::TryUlMuTransmission (Ptr<const WifiMacQueueItem> mpdu,
                                                   const WifiTxParameters& txParams)
{
  NS_LOG_FUNCTION (this << *mpdu << &txParams);
  NS_ASSERT (mpdu->GetHeader ().IsTrigger ());

  if (!m_sendMuRts)
    {
      // No protection because sending MU-RTS is disabled
      return std::unique_ptr<WifiProtection> (new WifiNoProtection ());
    }

  Ptr<ApWifiMac> apMac = DynamicCast<ApWifiMac> (m_mac);
  NS_ABORT_MSG_IF (apMac == 0, "HE APs only can send Trigger Frames");

  CtrlTriggerHeader trigger;
  mpdu->GetPacket ()->PeekHeader (trigger);

  WifiMuRtsCtsProtection* protection = new WifiMuRtsCtsProtection;
  // compute the TXVECTOR to use to send the MU-RTS Trigger Frame
  NS_ASSERT (apMac->GetStaList ().find (trigger.begin ()->GetAid12 ()) != apMac->GetStaList ().end ());
  Mac48Address staAddress = apMac->GetStaList ().find (trigger.begin ()->GetAid12 ())->second;
  protection->muRtsTxVector = m_mac->GetWifiRemoteStationManager ()->GetRtsTxVector (staAddress);
  // initialize the MU-RTS Trigger Frame
  // The Length, GI And LTF Type, MU-MIMO LTF Mode, Number Of HE-LTF Symbols, STBC,
  // LDPC Extra Symbol Segment, AP TX Power, Packet Extension, Spatial Reuse, Doppler
  // and UL HE-SIG-A2 Reserved subfields in the Common Info field are reserved.
  // (Sec. 9.3.1.23.4 of 802.11ax D3.0)
  protection->muRts.SetType (MU_RTS_TRIGGER);
  protection->muRts.SetUlBandwidth (txParams.m_txVector.GetChannelWidth ());

  for (const auto& userInfo : trigger)
    {
      // Add a User Info field for the new receiver
      // The MCS, Coding Type, DCM, SS Allocation and Target RSSI fields in the User Info
      // field are reserved (Sec. 9.3.1.23.4 of 802.11ax D3.0)
      CtrlTriggerUserInfoField& ui = protection->muRts.AddUserInfoField ();
      ui.SetAid12 (userInfo.GetAid12 ());
      // TODO Improve the way RU allocation is set. For the moment, it's the first 20 MHz channel
      ui.SetRuAllocation (HeRu::RuSpec {true, HeRu::RU_242_TONE, 1});
    }

  return std::unique_ptr<WifiMuRtsCtsProtection> (protection);
}

} //namespace ns3
