#!/bin/bash
self="${0##*/}"
fname="${self%%.*}"
rm ../*.pcap
rm trafficFile_${fname}*
rm ../position_file.txt
rm output*
START_ID=1

#ns-3 Settings
RngRun=1 #Random Seed [1,2,3,...,Iteration]
Iteration=1 #Iteration number
enablePcap=false #Packet Capture (ON:true / OFF:false)
simulationTime=5 #Simulation Duration[s]

#Traffic Settings
AC="BE" #Access Category
L4PROTO="UDP" #Traffic
baBufferSize=256
PKTSIZE=1000 #MSDU Size[Byte]
CBR=200 #Offered Load[Mbps]
beMaxAmsduSize=1500 #'BE' Max A-MSDU Size[Byte]
beMaxAmpduSize=65535 #'BE' Max A-MPDU Size[Byte]

#MAC,PHY Settings
MCSRate=0 #MCS
enableRts=false #Using RTS/CTS
beTxopLimit=0 #'BE' TXOP Limit
txPowerAP=20 #AP TX Power[dBm]
txPowerSTA=20 #STA TX Power[dBm]
beMsduLifetime=65535 #'BE' MSDU LifeTime
EdThresholdAP=-82 #AP CCA-ED Threshold[dBm]
EdThresholdSTA=-82 #STA CCA-ED Threshold[dBm]
obssPdThreshold=-82 #OBSS CCA-ED Threshold[dBm]
enableThresholdPreambleDetection=true 
useExplicitBar=false 
queueSize=65535
channelWidth=80 #Bandwidth

#OBSS Settings
nObss=0 #OBSS number
obssStandard=11ac #OBSS IEEE Standard
obssDlPayload=0 #OBSS DL Payload Size
obssDlRate=0 #OBSS DL offered load
obssUlPayload=$PKTSIZE #OBSS UL Payload Size
obssUlRate=$CBR #OBSS UL offered load
obssTxopLimit=$beTxopLimit #OBSS TXOP Limit
apTheta=0 #OBSS AP Position Setting


#Topology Settings
../waf
posfile="../position_file.txt" #Output Position File
PI=3.1416
apDistance=7 #AP distance[m]
postype="fixed" #Position -> fixed / disc (random)
placement="file" #Position allocation -> file
BSSradius=15 #BSS Range[m] 

#Sectorize Simulation
Groupnum=3 #Group num
#GroupSTAnum=10 #GroupSTAnum
d1=1 #STA x position (near AP:G1)
d2=7 #STA x position (middle AP:G2)
d3=15 #STA x position (far AP:G3)
#pertheta=$(echo "scale=6;2*$PI/$GroupSTAnum" | bc -l) 

#for x in {0..49};do #Add (use 500 Iteration) //Iteration = [1,2,..10] + x(0,1,2,...49)*10 = [1,2,3,...10,11,12,...490,491,...500]

for GroupSTAnum in 4;do #Add Group STA roop
#initalization (Each GroupSTAnum)
STAthetaG1=0 #STA position theta init value (G1)
STAthetaG2=0 #STA position theta init value (G2)
STAthetaG3=0 #STA position theta init value (G3)
pertheta=$(echo "scale=6;2*$PI/$GroupSTAnum" | bc -l) 

for nVhtStations in $((${GroupSTAnum}*${Groupnum}));do #STA num

END_ID=$nVhtStations
nStationsPerObss=${nVhtStations}

#AP/STA Position Setting
#--------------------------------------------------BSS-1------------------------------------------------
##  AP Setting
AP1posx=0  #BSS1 AP(x,y) = (0,0)
AP1posy=0
echo 1 >> ${posfile}          #BSS number
echo ${AP1posx} >> ${posfile} #posx
echo ${AP1posy} >> ${posfile} #posy
sector=0

## STA Setting
for n_index in $(seq 1 $nVhtStations);do
if [ "$postype" = "disc" ]; then
posr=$(echo "scale=2;$BSSradius*sqrt($RANDOM/32768)" | bc -l) #Random selection r
postheta=$(echo "scale=2;2*$PI*$RANDOM/32768" | bc -l) #Random selection theta 
poscos=$(echo "scale=2;c ($postheta)" | bc -l) #Calculate cos(theta)
possin=$(echo "scale=2;s ($postheta)" | bc -l) #Calculate sin(theta)
posx=$(echo "scale=2;$poscos*$posr+$AP1posx" | bc -l) #BSS1 STA(x,y) = (r*cos(theta)+APx, r*sin(theta)+APy)
posy=$(echo "scale=2;$possin*$posr+$AP1posy" | bc -l)

elif [ "$postype" = "fixed" ];then

if [ $n_index -le $GroupSTAnum ]; then #STA Index <= o
posx=$(echo "scale=2;$d1*c ($STAthetaG1)" | bc -l) #BSS1 near STA(x,y) = (d1*cos(2pi/STAnum),d1*sin(2pi/STAnum)) 
posy=$(echo "scale=2;$d1*s ($STAthetaG1)" | bc -l)
STAthetaG1=$(echo "scale=6;$STAthetaG1+$pertheta" | bc -l)

elif [ $n_index -le $(($GroupSTAnum*2)) ]; then
posx=$(echo "scale=2;$d2*c ($STAthetaG2)" | bc -l) #middle STA(x,y) = (d2*cos(2pi/STAnum),d2*sin(2pi/STAnum))
posy=$(echo "scale=2;$d2*s ($STAthetaG2)" | bc -l) 
STAthetaG2=$(echo "scale=6;$STAthetaG2+$pertheta" | bc -l)

else
posx=$(echo "scale=2;$d3*c ($STAthetaG2)" | bc -l) #far STA(x,y) = (d2*cos(2pi/STAnum),d2*sin(2pi/STAnum))
posy=$(echo "scale=2;$d3*s ($STAthetaG2)" | bc -l) 
STAthetaG2=$(echo "scale=6;$STAthetaG2+$pertheta" | bc -l)
fi

fi

echo ${n_index} >> ${posfile} #STA num
temp=$( printf "%02x" $((n_index)) )  
echo "00:00:00:00:00:"${temp} >> ${posfile}
echo ${posx} >> ${posfile} #posx
echo ${posy}  >> ${posfile} #posy
echo $sector  >> ${posfile} #sector ID
done
#-------------------------------------------------------------------------------------------------------


#--------------------------------------------------BSS-2------------------------------------------------
##  AP Setting
if [ $nObss -gt 0 ]; then
AP2posx=${apDistance} #BSS2 AP(x,y) = (d,0)
AP2posy=0
echo 2 >> ${posfile} 		#BSS number
echo ${AP2posx} >> ${posfile}  #posx
echo ${AP2posy} >> ${posfile}  #posy

##  STA Setting
for n_index in $(seq 1 $nVhtStations);do
if [ "$postype" = "disc" ]; then
posr=$(echo "scale=2;$BSSradius*sqrt($RANDOM/32768)" | bc -l) #Random selection r
postheta=$(echo "scale=2;2*$PI*$RANDOM/32768" | bc -l) #Random selection theta 
poscos=$(echo "scale=2;c ($postheta)" | bc -l) #Calculate cos(theta)
possin=$(echo "scale=2;s ($postheta)" | bc -l) #Calculate sin(theta)
posx=$(echo "scale=2;$poscos*$posr+$AP2posx" | bc -l) #BSS2 STA(x,y) = (r*cos(theta)+APx, r*sin(theta)+APy)
posy=$(echo "scale=2;$possin*$posr+$AP2posy" | bc -l)

elif [ "$postype" = "fixed" ];then
posx=${AP2posx} #BSS2 STA(x,y) = (d,r)
posy=$(echo "scale=2;$AP2posy+$BSSradius" | bc -l)
fi

echo ${n_index} >> ${posfile} #STA num
temp=$( printf "%02x" $((n_index+nVhtStations+1)) )  
echo "00:00:00:00:00:"${temp} >> ${posfile}
echo ${posx} >> ${posfile} #posx
echo ${posy}  >> ${posfile} #posy
echo $sector >> ${posfile} #sector ID
done
fi 
#-------------------------------------------------------------------------------------------------------

#--------------------------------------------------BSS-3------------------------------------------------
##  AP Setting
if [ $nObss -gt 0 ]; then
AP3posx=$(echo "scale=2;$apDistance*1/2" | bc -l) #BSS3 AP(x,y) = (d*1/2,d*sin(pi/3)) = (d=5->2.5,4.33)
AP3posy=$(echo "scale=2;$apDistance*s ($PI/3)" | bc -l)

echo 3 >> ${posfile}    #BSS number
echo ${AP3posx} >> ${posfile}  #posx
echo ${AP3posy} >> ${posfile} #posy

##  STA Setting
for n_index in $(seq 1 $nVhtStations);do
if [ "$postype" = "disc" ]; then
posr=$(echo "scale=2;$BSSradius*sqrt($RANDOM/32768)" | bc -l) #Random selection r
postheta=$(echo "scale=2;2*$PI*$RANDOM/32768" | bc -l) #Random selection theta
poscos=$(echo "scale=2;c ($postheta)" | bc -l) #Calculate cos(theta)
possin=$(echo "scale=2;s ($postheta)" | bc -l) #Calculate sin(theta)
posx=$(echo "scale=2;$poscos*$posr+$AP3posx" | bc -l) #BSS3 STA(x,y) = (r*cos(theta)+APx, r*sin(theta)+APy)
posy=$(echo "scale=2;$possin*$posr+$AP3posy" | bc -l)

elif [ "$postype" = "fixed" ];then
posx=${AP3posx} 			#BSS3 STA(x,y) = (d*1/2,d*sin(pi/3)+r) = (2.5,4.33+r)
posy=$(echo "scale=2;$AP3posy+$BSSradius" | bc -l)
fi

echo ${n_index} >> ${posfile} #STA num
temp=$( printf "%02x" $((n_index+nVhtStations+1)) )  
echo "00:00:00:00:00:"${temp} >> ${posfile}
echo ${posx} >> ${posfile} #posx
echo ${posy}  >> ${posfile} #posy
echo $sector >> ${posfile} #sector ID
done
fi 
#-------------------------------------------------------------------------------------------------------

#--------------------------------------------------BSS-4------------------------------------------------
##  AP Setting
if [ $nObss -gt 0 ]; then
AP4posx=$(echo "scale=2;$apDistance*1/2" | bc -l) #BSS4 AP(x,y) = (d*1/2,-d*sin(pi/3)) = (d=5->2.5,-4.33)
AP4posy=$(echo "scale=2;-$apDistance*s ($PI/3)" | bc -l)
echo 4 >> ${posfile}    #BSS number
echo ${AP4posx} >> ${posfile}  #posx
echo ${AP4posy} >> ${posfile} #posy

##  STA Setting
for n_index in $(seq 1 $nVhtStations);do
if [ "$postype" = "disc" ]; then
posr=$(echo "scale=2;$BSSradius*sqrt($RANDOM/32768)" | bc -l) #Random selection r
postheta=$(echo "scale=2;2*$PI*$RANDOM/32768" | bc -l) #Random selection theta
poscos=$(echo "scale=2;c ($postheta)" | bc -l) #Calculate cos(theta)
possin=$(echo "scale=2;s ($postheta)" | bc -l) #Calculate sin(theta)
posx=$(echo "scale=2;$poscos*$posr+$AP4posx" | bc -l) #BSS4 STA(x,y) = (r*cos(theta)+APx, r*sin(theta)+APy)
posy=$(echo "scale=2;$possin*$posr+$AP4posy" | bc -l)

elif [ "$postype" = "fixed" ];then
posx=${AP4posx} 			#BSS4 STA(x,y) = (d*1/2,d*sin(pi/3)+r) = (2.5,-4.33+r)
posy=$(echo "scale=2;$AP4posy+$BSSradius" | bc -l)
fi

echo ${n_index} >> ${posfile} #STA num
temp=$( printf "%02x" $((n_index+nVhtStations+1)) )  
echo "00:00:00:00:00:"${temp} >> ${posfile}
echo ${posx} >> ${posfile} #posx
echo ${posy}  >> ${posfile} #posy
echo $sector >> ${posfile} #sector ID
done
fi 
#-------------------------------------------------------------------------------------------------------

 
## Running
trafficFile=trafficFile_${fname}_${nVhtStations}_${MCSRate}
#echo $START_ID $END_ID $AC $L4PROTO DL $PKTSIZE CBR $CBR >> ${trafficFile}
echo $START_ID $END_ID $AC $L4PROTO UL $PKTSIZE CBR $CBR >> ${trafficFile}


for RngRun in $(seq 1 $Iteration);do 

#RngRun=$((RngRun + x * 10 )) #Add (use 500 Iteration)
outputFile=output_${fname}_${MCSRate}_${RngRun}_${nVhtStations}.txt


../waf --run  "wifi-ofdma --channelWidth=${channelWidth}--baBufferSize=${baBufferSize} --beMaxAmsduSize=${beMaxAmsduSize}  --enableRts=${enableRts}  --ccaThrSta=${EdThresholdSTA} --ccaThrAp=${EdThresholdAP} --nStationsPerObss=${nStationsPerObss} --obssTxopLimit=$channelWidth{obssTxopLimit} --obssUlRate=${obssUlRate} --obssDlRate=${obssDlRate} --obssUlPayload=${obssUlPayload} --obssDlPayload=${obssDlPayload} --obssStandard=${obssStandard} --nObss=${nObss} --apTheta=${apTheta} --apDistance=${apDistance} --rngInit=${RngRun},${RngRun} --useExplicitBar=${useExplicitBar} --beMsduLifetime=${beMsduLifetime} --queueSize=${queueSize}    --beTxopLimit=${beTxopLimit}   --powerSta=${txPowerSTA} --powerAp=${txPowerAP} --radius=${BSSradius} --enablePcap=${enablePcap}  --Rate=${MCSRate}  --simulationTime=${simulationTime}  --trafficFile=scratch/${trafficFile} --nVhtStations=${nVhtStations}" >> ${outputFile} &

echo Performing RngRun $RngRun for Number of STAs $nVhtStations
done #RngRun roop
wait
rm ${trafficFile}
rm ${posfile}


done #STA num roop

wait #Add Group STA roop
done #Add Group STA roop

#wait #Add (use 500 Iteration)
#done #Add x roop (use 500 Iteration)


