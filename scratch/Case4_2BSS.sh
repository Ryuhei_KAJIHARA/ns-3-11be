#!/bin/bash
self="${0##*/}"
fname="${self%%.*}"
rm ../*.pcap
rm trafficFile_${fname}*
rm ../position_file.txt
rm output*
START_ID=1

#ns-3 Settings
RngRun=1 #Random Seed [1,2,3,...,Iteration]
Iteration=1 #Iteration number
enablePcap=false #Packet Capture (ON:true / OFF:false)
simulationTime=50 #Simulation Duration[s]
startInterval=10 #Duration of the interval (ms) in which all apps can send the first packet (default=10)

#Traffic Settings
AC="BE" #Access Category
L4PROTO="UDP" #Traffic
baBufferSize=256 #Block Ack buffer size
PKTSIZE=1000 #MSDU Size [Byte]
CBR=200 #Offered Load [Mbps]
beMaxAmsduSize=1500 #Maximum A-MSDU size for BE [Byte]
beMaxAmpduSize=65535 #Maximum A-MPDU size for BE [Byte] (Case3:52000 Case4:65535)
beMsduLifetime=65535 #Maximum MSDU lifetime for BE in milliseconds
beTxopLimit=0 #TXOP duration for BE in microseconds
queueSize=65535 #Maximum size of a WifiMacQueue (packets) default=5000

#MAC,PHY Settings
MCSRate=0 #BSS1 MCS (Case3:8 Case4:0)
MCSRate2=0 #BSS2 MCS (Case3:0 Case4:0)
guardInterval=800 #Guard Interval (800, 1600, 3200) default=800
channelWidth=20 #Channel bandwidth (20, 40, 80, 160)
enableRts=false #Enable or disable RTS/CTS
txPowerAP=20 #Power of AP [dBm]
txPowerSTA=20 #Power of STA [dBm]
txGain=0.0 #Transmission gain [dBm] default=0.0
rxGain=0.0 #Reception gain [dB] default=0.0
EdThresholdAP=-82 #BSS1 AP CCA-ED Threshold[dBm] (Case3:-82 Case4:-82)
EdThresholdSTA=-82 #BSS1 STA CCA-ED Threshold[dBm] (Case3:-82 Case4:-82)
EdThresholdAP2=-62 #BSS2 AP CCA-ED Threshold[dBm] (Case3:-82 Case4:-62)
EdThresholdSTA2=-62 #BSS2 STA CCA-ED Threshold[dBm] (Case3:-82 Case4:-62)
enableThresholdPreambleDetection=true #Enable or disable threshold-based preamble detection (if not set, preamble detection is always successful)
useExplicitBar=false #Use explicit BAR after missed Block Ack

#OBSS Settings
nObss=1 #Number of overlapping BSSs
obssStandard=11ac #Standard (11ax, 11ac, 11n) used for each overlapping BSS
obssDlPayload=0 #Payload size of downlink packets in overlapping BSSs [bytes]
obssDlRate=0 #Aggregate downlink load in each overlapping BSS [Mbps] (OBSS DL offered load)
obssUlPayload=$PKTSIZE #Payload size of uplink packets in overlapping BSSs [bytes]
obssUlRate=$CBR #Aggregate uplink load in each overlapping BSS [Mbps] (OBSS UL offered load)
OBSSbeMaxAmpduSize=65535 #Maximum A-MPDU size for OBSSes
obssTxopLimit=$beTxopLimit #TXOP duration for OBSSs in microseconds
apTheta=0 #OBSS AP Position Setting


#Topology Settings
../waf
posfile="../position_file.txt" #Output Position File
PI=3.1416
apDistance=10 #AP distance[m] (Case3:10[m] Case4:20[m])
postype="fixed" #Position -> fixed / disc (random)
placement="file" #Position allocation -> file
BSSradius=1 #BSS1 Range[m] (Case3:8[m] Case4:1[m])
BSSradius2=1 #BSS2 Range[m] (Case3:1[m] Case4:1[m])

for nVhtStations in 1 5 10 15 20 25 30 35 40 45 50;do #STA num 

END_ID=$nVhtStations
nStationsPerObss=${nVhtStations}

#------------------------------------------------ BSS1 Setting ----------------------------------------------------------------------
##  AP Setting
AP1posx=0  #BSS1 AP(x,y) = (0,0)
AP1posy=0
echo 1 >> ${posfile}          #BSS number
echo ${AP1posx} >> ${posfile} #posx
echo ${AP1posy} >> ${posfile} #posy
sector=0

RANDOM=3 #BSS1 STA position Random Seed

## STA Setting
for n_index in $(seq 1 $nVhtStations);do
if [ "$postype" = "disc" ]; then
RANDVAR=$RANDOM #setting seed 
posr=$(echo "scale=2;$BSSradius*sqrt($RANDVAR/32768)" | bc -l) #Random selection r
RANDVAR=$RANDOM #setting seed
postheta=$(echo "scale=2;2*$PI*$RANDVAR/32768" | bc -l) #Random selection theta 
poscos=$(echo "scale=2;c ($postheta)" | bc -l) #Calculate cos(theta)
possin=$(echo "scale=2;s ($postheta)" | bc -l) #Calculate sin(theta)
posx=$(echo "scale=2;$poscos*$posr+$AP1posx" | bc -l) #BSS1 STA(x,y) = (r*cos(theta)+APx, r*sin(theta)+APy)
posy=$(echo "scale=2;$possin*$posr+$AP1posy" | bc -l)Case

elif [ "$postype" = "fixed" ];then
posx=${AP1posx}				#BSS1 STA(x,y) = (0,r1)
posy=$(echo "scale=2;$AP1posy+$BSSradius" | bc -l) 
fi

echo ${n_index} >> ${posfile} #STA num
temp=$( printf "%02x" $((n_index)) )  
echo "00:00:00:00:00:"${temp} >> ${posfile}
echo ${posx} >> ${posfile} #posx
echo ${posy}  >> ${posfile} #posy
echo $sector  >> ${posfile} #sector ID
done

#------------------------------------------------ BSS2 Setting ----------------------------------------------------------------------
##  AP Setting
if [ $nObss -gt 0 ]; then
AP2posx=${apDistance} #BSS2 AP(x,y) = (d,0)
AP2posy=0
echo 2 >> ${posfile} 		#BSS number
echo ${AP2posx} >> ${posfile}  #posx
echo ${AP2posy} >> ${posfile}  #posy

##  STA Setting
for n_index in $(seq 1 $nVhtStations);do
if [ "$postype" = "disc" ]; then
RANDVAR=$RANDOM #setting seed 
posr=$(echo "scale=2;$BSSradius2*sqrt($RANDVAR/32768)" | bc -l) #Random selection r
RANDVAR=$RANDOM #setting seed
postheta=$(echo "scale=2;2*$PI*$RANDVAR/32768" | bc -l) #Random selection theta 
poscos=$(echo "scale=2;c ($postheta)" | bc -l) #Calculate cos(theta)
possin=$(echo "scale=2;s ($postheta)" | bc -l) #Calculate sin(theta)
posx=$(echo "scale=2;$poscos*$posr+$AP2posx" | bc -l) #BSS2 STA(x,y) = (r*cos(theta)+APx, r*sin(theta)+APy)
posy=$(echo "scale=2;$possin*$posr+$AP2posy" | bc -l)

elif [ "$postype" = "fixed" ];then
posx=${AP2posx} #BSS2 STA(x,y) = (d,r2)
posy=$(echo "scale=2;$AP2posy+$BSSradius2" | bc -l)
fi

echo ${n_index} >> ${posfile} #STA num
temp=$( printf "%02x" $((n_index+nVhtStations+1)) )  
echo "00:00:00:00:00:"${temp} >> ${posfile}
echo ${posx} >> ${posfile} #posx
echo ${posy}  >> ${posfile} #posy
echo $sector >> ${posfile} #sector ID
done
fi 

#------------------------------------------------ Simulation ----------------------------------------------------------------------
trafficFile=trafficFile_${fname}_${nVhtStations}_${MCSRate}
echo $START_ID $END_ID $AC $L4PROTO UL $PKTSIZE CBR $CBR >> ${trafficFile}


for RngRun in $(seq 1 $Iteration);do 

outputFile=output_${fname}_${MCSRate}_${RngRun}_${nVhtStations}.txt


../waf --run  "wifi-ofdma --placement=${placement} --baBufferSize=${baBufferSize} --beMaxAmsduSize=${beMaxAmsduSize} --beMaxAmpduSize=${beMaxAmpduSize} --obssMaxAmpduSize=${OBSSbeMaxAmpduSize} --enableRts=${enableRts}  --ccaThrSta=${EdThresholdSTA} --ccaThrAp=${EdThresholdAP} --ccaThrSta2=${EdThresholdSTA2} --ccaThrAp2=${EdThresholdAP2} --nStationsPerObss=${nStationsPerObss} --obssTxopLimit=${obssTxopLimit} --obssUlRate=${obssUlRate} --obssDlRate=${obssDlRate} --obssUlPayload=${obssUlPayload} --obssDlPayload=${obssDlPayload} --obssStandard=${obssStandard} --nObss=${nObss} --apTheta=${apTheta} --apDistance=${apDistance} --rngInit=${RngRun},${RngRun} --useExplicitBar=${useExplicitBar} --beMsduLifetime=${beMsduLifetime} --queueSize=${queueSize}    --beTxopLimit=${beTxopLimit}   --powerSta=${txPowerSTA} --powerAp=${txPowerAP} --txGain=${txGain} --rxGain=${rxGain} --radius=${BSSradius} --enablePcap=${enablePcap} --Rate=${MCSRate} --Rate2=${MCSRate2}  --simulationTime=${simulationTime}  --trafficFile=scratch/${trafficFile} --nVhtStations=${nVhtStations} --enableThresholdPreambleDetection=${enableThresholdPreambleDetection} --startInterval=${startInterval} --guardInterval=${guardInterval} --channelWidth=${channelWidth}" >> ${outputFile} &

echo Performing RngRun $RngRun for Number of STAs $nVhtStations
done #RngRun roop
wait
rm ${trafficFile}
rm ${posfile}


done 
