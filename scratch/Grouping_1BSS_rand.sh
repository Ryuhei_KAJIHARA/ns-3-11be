#!/bin/bash
self="${0##*/}"
fname="${self%%.*}"
rm ../*.pcap
rm trafficFile_${fname}*
#rm ../position_file.txt
rm output*
START_ID=1

#ns-3 Settings
RngRun=1 #Random Seed [1,2,3,...,Iteration]
Iteration=10 #Iteration number
enablePcap=false #Packet Capture (ON:true / OFF:false)
simulationTime=5 #Simulation Duration[s] (MCS0:5[s] MCS4:20[s] MCS8:50[s])

#Traffic Settings
AC="BE" #Access Category
L4PROTO="UDP" #Traffic
baBufferSize=256
PKTSIZE=1000 #MSDU Size[Byte]
CBR=200 #Offered Load[Mbps]
beMaxAmsduSize=1500 #'BE' Max A-MSDU Size[Byte]
beMaxAmpduSize=65535 #'BE' Max A-MPDU Size[Byte]

#MAC,PHY Settings
MCSRate=0 #MCS
enableRts=false #Using RTS/CTS
beTxopLimit=0 #'BE' TXOP Limit
txPowerAP=20 #AP TX Power[dBm]
txPowerSTA=20 #STA TX Power[dBm]
beMsduLifetime=65535 #'BE' MSDU LifeTime
EdThresholdAP=-82 #AP CCA-ED Threshold[dBm]
EdThresholdSTA=-82 #STA CCA-ED Threshold[dBm]
obssPdThreshold=-82 #OBSS CCA-ED Threshold[dBm]
enableThresholdPreambleDetection=true 
useExplicitBar=false 
queueSize=65535

#OBSS Settings
nObss=0 #OBSS number
obssStandard=11ac #OBSS IEEE Standard
obssDlPayload=0 #OBSS DL Payload Size
obssDlRate=0 #OBSS DL offered load
obssUlPayload=$PKTSIZE #OBSS UL Payload Size
obssUlRate=$CBR #OBSS UL offered load
obssTxopLimit=$beTxopLimit #OBSS TXOP Limit
apTheta=0 #OBSS AP Position Setting


#Topology Settings
../waf
posfile="../position_file.txt" #Output Position File
PI=3.1416
apDistance=7 #AP distance[m]
postype="disc" #Create Position file -> fixed / disc (random)
placement="disc" #Position allocation -> disc (Create position in wifi-ofdma.cc)
BSSradius=3 #BSS Range[m] 

#for x in {0..49};do #Add (use 500 Iteration) //Iteration = [1,2,..10] + x(0,1,2,...49)*10 = [1,2,3,...10,11,12,...490,491,...500]

for nVhtStations in 3 15 30 45 60 75 90 105;do #STA num Ex.[for nVhtStations in 1 5 10 15 20 25 30;do]
#for nVhtStations in 120 135 150 165 180 195 210;do

END_ID=$nVhtStations
nStationsPerObss=${nVhtStations}

## Running
trafficFile=trafficFile_${fname}_${nVhtStations}_${MCSRate}
#echo $START_ID $END_ID $AC $L4PROTO DL $PKTSIZE CBR $CBR >> ${trafficFile}
echo $START_ID $END_ID $AC $L4PROTO UL $PKTSIZE CBR $CBR >> ${trafficFile}


for RngRun in $(seq 1 $Iteration);do 

#RngRun=$((RngRun + x * 10 )) #Add (use 500 Iteration)
outputFile=output_${fname}_${MCSRate}_${RngRun}_${nVhtStations}.txt


../waf --run  "wifi-ofdma  --placement=${placement} --baBufferSize=${baBufferSize} --beMaxAmsduSize=${beMaxAmsduSize} --beMaxAmpduSize=${beMaxAmpduSize} --enableRts=${enableRts}  --ccaThrSta=${EdThresholdSTA} --ccaThrAp=${EdThresholdAP} --nStationsPerObss=${nStationsPerObss} --obssTxopLimit=${obssTxopLimit} --obssUlRate=${obssUlRate} --obssDlRate=${obssDlRate} --obssUlPayload=${obssUlPayload} --obssDlPayload=${obssDlPayload} --obssStandard=${obssStandard} --nObss=${nObss} --apTheta=${apTheta} --apDistance=${apDistance} --rngInit=${RngRun},${RngRun} --useExplicitBar=${useExplicitBar} --beMsduLifetime=${beMsduLifetime} --queueSize=${queueSize}    --beTxopLimit=${beTxopLimit}   --powerSta=${txPowerSTA} --powerAp=${txPowerAP} --radius=${BSSradius} --enablePcap=${enablePcap}  --Rate=${MCSRate}  --simulationTime=${simulationTime}  --trafficFile=scratch/${trafficFile} --nVhtStations=${nVhtStations}" >> ${outputFile} &

echo Performing RngRun $RngRun for Number of STAs $nVhtStations
done #RngRun roop
wait
rm ${trafficFile}
#rm ${posfile}


done #STA num roop

#wait #Add (use 500 Iteration)
#done #Add x roop (use 500 Iteration)


