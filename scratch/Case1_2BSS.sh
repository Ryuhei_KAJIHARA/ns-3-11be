#!/bin/bash
self="${0##*/}"
fname="${self%%.*}"
rm ../*.pcap
rm trafficFile_${fname}*
rm ../position_file.txt
rm output*
START_ID=1

#ns-3 Settings
RngRun=1 #Random Seed [1,2,3,....,Iteration]
Iteration=10 #Iteration number
enablePcap=false #Enable PCAP trace file generation (ON:true / OFF:false)
simulationTime=50 #Simulation time in seconds
startInterval=10 #Duration of the interval (ms) in which all apps can send the first packet (default=10)

#Traffic Settings
AC="BE" #Access Category
L4PROTO="UDP" #Traffic type
baBufferSize=256 #Block Ack buffer size
PKTSIZE=1000 #MSDU Size [Byte]
CBR=200 #Offered Load [Mbps]
beMaxAmsduSize=1500 #Maximum A-MSDU size for BE [Byte]
beMaxAmpduSize=65535 #Maximum A-MPDU size for BE [Byte]
beMsduLifetime=65535 #Maximum MSDU lifetime for BE in milliseconds
beTxopLimit=0 #TXOP duration for BE in microseconds
queueSize=65535 #Maximum size of a WifiMacQueue (packets) default=5000 

#MAC,PHY Settings
MCSRate=8 #MCS
guardInterval=800 #Guard Interval (800, 1600, 3200) default=800
channelWidth=20 #Channel bandwidth (20, 40, 80, 160)
enableRts=false #Enable or disable RTS/CTS
txPowerAP=20 #Power of AP [dBm]
txPowerSTA=20 #Power of STA [dBm]
txGain=0.0 #Transmission gain [dBm] default=0.0
rxGain=0.0 #Reception gain [dB] default=0.0
EdThresholdAP=-82 #CCA Threshold of AP [dBm]
EdThresholdSTA=-82 #CCA Threshold of STA [dBm]
rxSensitivity=-91 #Receiver Sensitivity [dBm] default=-91
enableThresholdPreambleDetection=true #Enable or disable threshold-based preamble detection (if not set, preamble detection is always successful)
useExplicitBar=false #Use explicit BAR after missed Block Ack

#OBSS Settings
nObss=1 #Number of overlapping BSSs
obssStandard=11ac #Standard (11ax, 11ac, 11n) used for each overlapping BSS
obssDlPayload=0 #Payload size of downlink packets in overlapping BSSs [bytes]
obssDlRate=0 #Aggregate downlink load in each overlapping BSS [Mbps] (OBSS DL offered load)
obssUlPayload=$PKTSIZE #Payload size of uplink packets in overlapping BSSs [bytes]
obssUlRate=$CBR #Aggregate uplink load in each overlapping BSS [Mbps] (OBSS UL offered load)
obssMaxAmpduSize=$beMaxAmpduSize #Maximum A-MPDU size for OBSSes
obssTxopLimit=$beTxopLimit #TXOP duration for OBSSs in microseconds

#Topology Settings
../waf
posfile="../position_file.txt" #Output Position File
PI=3.1416
BSSradius=8 #Radius of the disc centered in the AP and containing all the non-AP STAs
apDistance=5 #Comma separated (no spaces) list of distances for OBSS APs (Ex. 7,7)
apTheta=0 #Comma separated (no spaces) list of angles for OBSS APs (Ex. 0,60)
postype="fixed" #Position type -> fixed or disc (random)
placement="file" #Create position -> fixed (Create position in this file) or file (Create position in this file) or disc (Create position in wifi-ofdma.cc)

for nVhtStations in 1 5 10 15 20 25 30 35 40 45 50; #STA num
do

END_ID=$nVhtStations
nStationsPerObss=${nVhtStations}

#------------------------------------------------ BSS1 Setting ----------------------------------------------------------------------
AP1posx=0  #BSS1 AP(x,y) = (0,0)
AP1posy=0

echo 1 >> ${posfile}          #BSS number
echo ${AP1posx} >> ${posfile} #posx
echo ${AP1posy} >> ${posfile} #posy
sector=0

for n_index in $(seq 1 $nVhtStations);do
if [ "$postype" = "disc" ]; then
posr=$(echo "scale=2;$BSSradius*sqrt($RANDOM/32768)" | bc -l)
postheta=$(echo "scale=2;2*$PI*$RANDOM/32768" | bc -l)
poscos=$(echo "scale=2;c ($postheta)" | bc -l)
possin=$(echo "scale=2;s ($postheta)" | bc -l)
posx=$(echo "scale=2;$poscos*$posr" | bc -l)
posy=$(echo "scale=2;$possin*$posr" | bc -l)

elif [ "$postype" = "fixed" ];then
posx=$AP1posx
posy=$(echo "scale=2;$AP1posy+$BSSradius" | bc -l) #(x,y) = (0,r)
fi

echo ${n_index} >> ${posfile} #STA num
temp=$( printf "%02x" $((n_index)) )  
echo "00:00:00:00:00:"${temp} >> ${posfile}
echo ${posx} >> ${posfile} #posx
echo ${posy}  >> ${posfile} #posy
echo $sector  >> ${posfile} #sector ID
done

#------------------------------------------------ BSS2 Setting ----------------------------------------------------------------------
if [ $nObss -gt 0 ]; then
AP2posx=${apDistance} #BSS2 AP(x,y) = (d,0)
AP2posy=0

echo 2 >> ${posfile} 	#BSS number
echo ${AP2posx} >> ${posfile}  #posx
echo ${AP2posy} >> ${posfile}  #posy

for n_index in $(seq 1 $nVhtStations);do
if [ "$postype" = "disc" ]; then
posr=$(echo "scale=2;$BSSradius*sqrt($RANDOM/32768)" | bc -l)
postheta=$(echo "scale=2;2*$PI*$RANDOM/32768" | bc -l)
poscos=$(echo "scale=2;c ($postheta)" | bc -l)
possin=$(echo "scale=2;s ($postheta)" | bc -l)
posx=$(echo "scale=2;$poscos*$posr+$AP2posx" | bc -l)
posy=$(echo "scale=2;$poscos*$posr" | bc -l)

elif [ "$postype" = "fixed" ];then
posx=$(echo "scale=2;$AP2posx" | bc -l) #(x,y) = (d,r)
posy=$(echo "scale=2;$AP2posy+$BSSradius" | bc -l)
fi

echo ${n_index} >> ${posfile} #STA num
temp=$( printf "%02x" $((n_index+nVhtStations+1)) )  
echo "00:00:00:00:00:"${temp} >> ${posfile}
echo ${posx} >> ${posfile} #posx
echo ${posy}  >> ${posfile} #posy
echo $sector >> ${posfile} #sector ID
done
fi 

#-------------------------------------------- Simulation -----------------------------------------------------------------------

trafficFile=trafficFile_${fname}_${nVhtStations}_${MCSRate}
echo $START_ID $END_ID $AC $L4PROTO UL $PKTSIZE CBR $CBR >> ${trafficFile}

for RngRun in $(seq 1 $Iteration);do

outputFile=output_${fname}_${MCSRate}_${RngRun}_${nVhtStations}.txt

../waf --run  "wifi-ofdma  --placement=${placement} --baBufferSize=${baBufferSize} --beMaxAmsduSize=${beMaxAmsduSize} --beMaxAmpduSize=${beMaxAmpduSize} --obssMaxAmpduSize=${obssMaxAmpduSize} --enableRts=${enableRts}  --ccaThrSta=${EdThresholdSTA} --ccaThrAp=${EdThresholdAP} --rxSensitivity=${rxSensitivity} --enableThresholdPreambleDetection=${enableThresholdPreambleDetection} --nStationsPerObss=${nStationsPerObss} --obssTxopLimit=${obssTxopLimit} --obssUlRate=${obssUlRate} --obssDlRate=${obssDlRate} --obssUlPayload=${obssUlPayload} --obssDlPayload=${obssDlPayload} --obssStandard=${obssStandard} --nObss=${nObss} --apTheta=${apTheta} --apDistance=${apDistance} --rngInit=${RngRun},${RngRun} --useExplicitBar=${useExplicitBar} --beMsduLifetime=${beMsduLifetime} --queueSize=${queueSize}    --beTxopLimit=${beTxopLimit}   --powerSta=${txPowerSTA} --powerAp=${txPowerAP} --txGain=${txGain} --rxGain=${rxGain} --radius=${BSSradius} --enablePcap=${enablePcap}  --Rate=${MCSRate}  --simulationTime=${simulationTime}  --trafficFile=scratch/${trafficFile} --nVhtStations=${nVhtStations} --guardInterval=${guardInterval} --startInterval=${startInterval} --channelWidth=${channelWidth}" >> ${outputFile} &

echo Performing RngRun $RngRun for Number of STAs $nVhtStations
done
wait
rm ${trafficFile}
rm  ${posfile}

done

