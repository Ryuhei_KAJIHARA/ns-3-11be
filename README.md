# 修論に使用したns-3 11acシミュレータ

修論に使用した11acのシミュレータです．マルチBSS環境のCase1〜4とグルーピングのシミュレーションが可能です．
ns-3のバージョンはns-3-11beです．

このリポジトリに関する質問等は下記メールアドレスまでお願いします．なお，シェルスクリプトの中身のパラメータについてなどは修論の付録に付けているのでそちらを参照してください．

※[at]はアットマーク

kajihara.ryuhei235[at]mail.kyutech.jp

## 使い方
### 1. ns-3のクローン 
**日本語を含まない**フォルダを作成し，その中でこのプロジェクトをクローンしてください．

``` $ git clone https://gitlab.com/Ryuhei_KAJIHARA/ns-3-11be.git ```

### 2. ns-3のビルド
クローン完了後，ns-3-11beというフォルダが作成されるので，その中でns-3のビルドを行います．

``` ns-3-11be$ ./waf configure --build-profile=optimized ```

問題なければビルドが行われ，以下のような画面が出ます．

```
Setting top to                           : /home/user/test/ns-3-11be 
Setting out to                           : /home/user/test/ns-3-11be/build 
Checking for 'gcc' (C compiler)          : /home/user/anaconda3/bin/x86_64-conda_cos6-linux-gnu-cc 
===================================================
略
==================================================
Use sudo to set suid bit      : not enabled (option --enable-sudo not selected)
XmlIo                         : enabled
'configure' finished successfully (40.964s)
```
※ビルドに失敗し，再度ビルドを行う際には，**ns-3-11beフォルダ内のBuildフォルダを削除してから**再度ビルドを行ってください．

### 3. ns-3のコンパイル
ns-3のコンパイルを下記コマンドで行います．

``` ns-3-11be$ ./waf ```

問題なければコンパイルが行われ，以下のような画面が出ます．これでns-3を使用する準備は完了しました．
```
Waf: Entering directory `/home/user/test/ns-3-11be/build'
[   1/1986] Compiling install-ns3-header: ns3/isotropic-antenna-model.h
[   2/1986] Compiling install-ns3-header: ns3/cosine-antenna-model.h
===================================================
略
==================================================
[1996/1997] Compiling src/tap-bridge/model/tap-creator.cc
[1997/1997] Linking build/src/tap-bridge/ns3-dev-tap-creator-optimized
Waf: Leaving directory `/home/user/test/ns-3-11be/build'
Build commands will be stored in build/compile_commands.json
'build' finished successfully (51.620s)

Modules built:
antenna                   aodv                      applications
bridge                    buildings                 config-store
core                      csma                      csma-layout
dsdv                      dsr                       energy
fd-net-device             flow-monitor              internet
internet-apps             lr-wpan                   lte
mesh                      mobility                  netanim
network                   nix-vector-routing        olsr
point-to-point            point-to-point-layout     propagation
sixlowpan                 spectrum                  stats
tap-bridge                test (no Python)          topology-read
traffic-control           uan                       virtual-net-device
wave                      wifi                      wimax

Modules not built (see ns-3 tutorial for explanation):
brite                     click                     dpdk-net-device
mpi                       openflow                  visualizer
```
※こちらでシミュレーションまでうまくいくことは確認していますが，もしコンパイルに失敗する場合は，問い合わせをお願いします．

### 4. ns-3のシミュレーション

シミュレーションに使用するシェルスクリプトは\scratch内にあります．
シミュレーションを行う際には下記のように実行します．

``` scratch$ ./Case1_2BSS.sh ```

シミュレーションが完了するとoutput...といったテキストファイルが出力され、スループットなどの情報が記録されます．**なお，テキストファイルは実行ごとに削除されるので保存する場合はどこかに退避させてください．**

### 5. 修論のシミュレーション
- 対称マルチBSS環境 (Case1,2)

    scratch内のCase1_2BSS.shおよびCase2_2BSS.shでシミュレーションを行うことができます．3BSSのパターンも置いてあります．

- 非対称マルチBSS環境 (Case3,4)

    scratch内のCase3_2BSS.shおよびCase4_2BSS.shでシミュレーションを行うことができます．しかし，これらは非対称マルチBSS環境であるため，キャリアセンスの閾値をBSSごとに変える必要があります．この設定はデフォルトのwifi-ofdma.cc（パラメータの設定を行うファイル）には無いので記述を少し変更した**別のccファイルを使う必要があります．**

    148NASの\share\home_卒業生データ(2019~)\ochi_lab\2019\kajihara\research\M2_thesis\data\ns-3-11be_code内にwifi-ofdma_Case3_4.ccを使用してください．
    ※ns-3内の「wifi-ofdma.cc」を「wifi-ofdma_original.cc」等に名称変更し，「wifi-ofdma_Case3_4.cc」を「wifi-ofdma.cc」に変更してシミュレーションを行ってください．

    
